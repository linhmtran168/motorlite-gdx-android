/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.androidgame.motorlitegdx.mobile;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
    }
    public static final class id {
        public static final int RelativeLayout1=0x7f050008;
        public static final int active_image=0x7f050001;
        public static final int back_button=0x7f050016;
        public static final int best_time=0x7f05001a;
        public static final int button_set=0x7f05000f;
        public static final int change_profile_button=0x7f05000a;
        public static final int confirm_button=0x7f050017;
        public static final int create_profile_button=0x7f05001f;
        public static final int delete_button=0x7f05001e;
        public static final int hello_text=0x7f050010;
        public static final int intrucstion_button=0x7f05000e;
        public static final int lose=0x7f050019;
        public static final int lose_num=0x7f050005;
        public static final int main_menu_button=0x7f050012;
        public static final int multi_button=0x7f05000c;
        public static final int new_profile_label=0x7f050014;
        public static final int note_text=0x7f050011;
        public static final int notify_text=0x7f05001c;
        public static final int online_id=0x7f050007;
        public static final int online_players=0x7f050013;
        public static final int online_stat_button=0x7f050021;
        public static final int player_name=0x7f050006;
        public static final int profile_input=0x7f050015;
        public static final int profile_label=0x7f050000;
        public static final int profile_name=0x7f050003;
        public static final int rank=0x7f050002;
        public static final int set_active_button=0x7f05001d;
        public static final int splash_text=0x7f050020;
        public static final int start_button=0x7f05000b;
        public static final int statistic_button=0x7f05000d;
        public static final int top_player_text=0x7f05001b;
        public static final int welcome_text=0x7f050009;
        public static final int win=0x7f050018;
        public static final int win_num=0x7f050004;
    }
    public static final class layout {
        public static final int fade_in=0x7f030000;
        public static final int fade_out=0x7f030001;
        public static final int list_row=0x7f030002;
        public static final int list_row_2=0x7f030003;
        public static final int list_row_3=0x7f030004;
        public static final int main_menu=0x7f030005;
        public static final int multiplay_start=0x7f030006;
        public static final int new_profile=0x7f030007;
        public static final int online_statistic=0x7f030008;
        public static final int profile_detail=0x7f030009;
        public static final int profile_list=0x7f03000a;
        public static final int splash_screen=0x7f03000b;
        public static final int statistic=0x7f03000c;
    }
    public static final class string {
        public static final int accept=0x7f040029;
        public static final int activate_profile=0x7f040016;
        public static final int app_name=0x7f040001;
        public static final int back=0x7f04001a;
        public static final int best_time=0x7f040015;
        public static final int busy_message=0x7f04002d;
        public static final int cancel=0x7f04002a;
        public static final int change_profile=0x7f04000f;
        public static final int confirm=0x7f040004;
        public static final int connect_request=0x7f040027;
        public static final int connection_problem=0x7f04001f;
        public static final int connection_problem_message=0x7f040020;
        public static final int decline=0x7f040028;
        public static final int delete_problem=0x7f040021;
        public static final int delete_problem_message=0x7f040022;
        public static final int delete_profile=0x7f040017;
        public static final int discard=0x7f040009;
        public static final int empty_profile_name=0x7f040005;
        public static final int empty_profile_name_message=0x7f040006;
        public static final int hello=0x7f040000;
        public static final int hello_player=0x7f040024;
        public static final int instruction=0x7f04000d;
        public static final int lose=0x7f040014;
        public static final int main_menu=0x7f040003;
        public static final int multiplay=0x7f040023;
        public static final int new_profile=0x7f040018;
        public static final int new_profile_label=0x7f040002;
        public static final int no_profile=0x7f040019;
        public static final int note_text=0x7f040025;
        public static final int notify_text=0x7f04001b;
        public static final int online_players=0x7f040026;
        public static final int online_statistic=0x7f04001c;
        public static final int play=0x7f04002b;
        public static final int profile_exists=0x7f040010;
        public static final int profile_exists_message=0x7f040011;
        public static final int profile_name=0x7f040012;
        public static final int rank=0x7f04001d;
        public static final int reply=0x7f04002c;
        public static final int splash_screen_here=0x7f04000e;
        public static final int start_game=0x7f04000b;
        public static final int statistic=0x7f04000c;
        public static final int top_players=0x7f04001e;
        public static final int unsaved_changes=0x7f040007;
        public static final int unsaved_changes_message=0x7f040008;
        public static final int welcome=0x7f04000a;
        public static final int win=0x7f040013;
    }
}
