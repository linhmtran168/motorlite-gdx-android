package com.androidgame.motorlitegdx;

import java.util.Random;


/**
 * Class for AI motor simulation
 * @author TheEmperor
 *
 */
public class AIMotor extends Motor {
	Random rand = new Random();
	private static final float TICK_AI = 1f;
	float tickTimeAI;
	
	public AIMotor(float x, float y, int type) {
		super(x, y, type);
		tickTimeAI = 0;
	}
	
	public void update(float deltaTime, Motor player) {
		super.update(deltaTime);
		
		// Fixed time step simulation for AI player (AI playerer lift and hinder only after TICK_AI seconds)
		tickTimeAI += deltaTime;
		while (tickTimeAI > TICK_AI) {
			tickTimeAI -= TICK_AI;
			lift();
			if (rand.nextInt(100) < 2) hinder(player);
		}
	}
	
	@Override
	public void lift() {
		if (rand.nextFloat() > 0.9f) {
			super.lift();
		}
	}
	
	/**
	 * AI motor evade other motor or obstacle
	 * @param direct
	 */
	public void evade(int direct) {
		if (rand.nextFloat() <= 0.5f) {
			super.changeLane(direct);
		}
	}
	
	/**
	 * AI motor hinder other motor
	 * @param player Motor other motor needed to be hindered
	 */
	public void hinder(Motor player) {
		float gapX = position.x - player.position.x;
		float gapY = position.y - player.position.y;
		
		// Only hinder when 2 motors are not in the same lane
		if (gapX >= 0 && gapX <= 5 && gapY != 0 && gapY != 0.4f && gapY != -0.4f) {
			// If the y coordinate of the 2 motors are the same, the chance of doing this is only 50%
			float chance = gapX == 0 ? 0.9f : 0.5f;
			if (rand.nextFloat() >= chance) {
				if (position.y >= 5.6f) {
					position.y -= 2f;
				} else {
					position.y += 2f;
				}
			}
		}
	}
}
