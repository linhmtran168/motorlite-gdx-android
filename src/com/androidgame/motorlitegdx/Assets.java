package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.Animation;
import com.androidgame.motorlitegdx.helpers.TextureFont;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Class to load assets for the game
 * @author TheEmperor
 *
 */
public class Assets {
    public static Texture[] maps;
    public static TextureRegion[] mapRegions;
    
    public static Texture items;        
    public static TextureRegion nails;
    public static TextureRegion can;
    public static TextureRegion brick;
    public static TextureRegion milestone;
    public static TextureRegion flag;
    public static TextureRegion minimap;
    public static TextureRegion mapPlayer;
    public static TextureRegion mapAI;
    
    public static Texture menus;
    public static TextureRegion pausedMenu;
    public static TextureRegion gameOverMenu;
    
    public static TextureRegion pausedButton;
    public static TextureRegion liftButton;
    public static TextureRegion joystickOut;
    public static TextureRegion joystickIn;
    
    public static TextureRegion[] readyMenu;
    public static Texture motors;
    public static Animation motor;
    public static Animation aiMotor;
    
    public static Texture fixedFontTexture;
    public static BitmapFont speedFont;
    public static BitmapFont textFont;
    public static TextureFont milestoneNumber;
    
    public static Music music;
    public static Sound sound;
    
//    public static Font font;
//    public static Font speedFont;
    
    /**
     * Load all the game assets
     */
    public static void load() {
    	// Load map texture
    	maps  = new Texture[5];
    	for (int i = 0; i < 5; i++) {
	        maps[i] = loadTexture("data/mapnew_0" + (i+1) +".png");
    	}
    	
    	// Load map regions
    	mapRegions = new TextureRegion[5];
    	for (int i = 0; i< 5; i++) {
    		mapRegions[i] = new TextureRegion(maps[i], 0, 0, 1024, 480);
    	}
        
    	// Load in-game item texture
        items = loadTexture("data/items.png");        
        brick = new TextureRegion(items, 0, 0, 64, 64);
        can = new TextureRegion(items, 64, 0, 64, 64);
        nails = new TextureRegion(items, 128, 0, 64, 32);
        flag = new TextureRegion(items, 0, 64, 480, 448);
        milestone = new TextureRegion(items, 192, 0, 64, 64);
        minimap = new TextureRegion(items, 256, 0, 640, 44);
        mapPlayer = new TextureRegion(items, 896, 0, 10, 10);
        mapAI = new TextureRegion(items, 928, 0, 10, 10);
        
        // Load menu texture
        menus = loadTexture("data/menu.png");
        pausedMenu = new TextureRegion(menus, 0, 0, 144, 288);
        gameOverMenu = new TextureRegion(menus, 160, 0, 144, 288);
        
        // Load button and joystick
        pausedButton = new TextureRegion(menus, 384, 0, 64, 64);
        liftButton = new TextureRegion(menus, 320, 0, 64, 64);
        joystickOut = new TextureRegion(items, 480, 64, 192, 192);
        joystickIn = new TextureRegion(items, 680, 72, 80, 80);
        
        
        readyMenu = new TextureRegion[3];
        for (int i = 0; i < 3; i++) {
        	readyMenu[i] = new TextureRegion(menus, 160*i, 288, 144, 224);
        }
        
        
        // Load motor texture and create animation object from thame
        motors = loadTexture("data/motors.png");
        motor = new Animation(0.01f,
        		new TextureRegion(motors, 0, 0, 160, 160),
        		new TextureRegion(motors, 160, 0, 160, 160)
        );
        aiMotor = new Animation(0.01f,
        		new TextureRegion(motors, 0, 160, 160, 160),
        		new TextureRegion(motors, 160, 160, 160, 160)
       ); 
        
        // Load and create font
        speedFont = new BitmapFont(Gdx.files.internal("data/quartzms.fnt"), false);
        textFont = new BitmapFont(Gdx.files.internal("data/mistral.fnt"), false);
        fixedFontTexture = loadTexture("data/mistral301628.png"); 
        milestoneNumber = new TextureFont(fixedFontTexture, 0, 0, 16, 16, 28);
        
    }       
    
    /**
     * Load a texture from an image file
     * @param file String name of the image file
     * @return
     */
    public static Texture loadTexture (String file) {
		return new Texture(Gdx.files.internal(file));
	}
    
    public static void playSound(Sound sound) {
        if(Settings.soundEnabled)
            sound.play(1);
    }
}
