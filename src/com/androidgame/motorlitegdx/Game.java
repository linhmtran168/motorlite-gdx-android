package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.Screen;
import com.androidgame.motorlitegdx.mobile.helpers.ActionResolverAndroid;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;


/**
 * The main game class for the Game
 * @author TheEmperor
 *
 */
public abstract class Game implements ApplicationListener {
	Screen screen;
	ActionResolverAndroid actionResolver;
	
	public Game(ActionResolverAndroid actionResolver) {
		this.actionResolver = actionResolver;
	}

	public void setScreen (Screen newScreen) {
		screen.pause();
		screen.dispose();
		screen = newScreen;
	}

	public abstract Screen getStartScreen ();

	public void create () {
		screen = getStartScreen();
	}

	public void resume () {
		screen.resume();
	}

	public void render () {
		screen.update(Gdx.graphics.getDeltaTime());
		screen.present(Gdx.graphics.getDeltaTime());
	}

	public void resize (int width, int height) {

	}

	public void pause () {
		screen.pause();
	}

	public void dispose () {
		screen.dispose();
	}
}
