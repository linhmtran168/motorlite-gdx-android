package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.DynamicGameObject;
import com.badlogic.gdx.math.Rectangle;


/**
 * Class for motor simulation
 * @author TheEmperor
 *	
 */
public class Motor extends DynamicGameObject {
	public static final float WIDTH = 4.5f;
	public static final float HEIGHT = 4.5f;
	public static final float MAX_VELOCITY = 30;
	public static final float MIN_VELOCITY = 5;
	public static final float ACCEL = 4f;
	private static final float TICK_HIT = 0.2f;
	public boolean isLiftUp;
	public boolean isReachGoal;
	public boolean isStopped;
	public boolean lastLift;
	public boolean isSlowed;
	public int timePast;
	public float startTime;
	public int place;
	public int motorType;
	public float tickTime;
//	public int state;
	public float stateTime;
	public float startTimeNano;
	public float timePastNano;
	
	/**
	 * Motor class constructor
	 * @param x Float x co-ordinate
	 * @param y Float y co-ordinate
	 * @param type Int type of the motor (image using to display the motor)
	 */
	public Motor(float x, float y, int type) {
		super(x, y, new Rectangle(x - WIDTH/2 + 1f, y - HEIGHT/2, WIDTH - 1f, 1f));
		isLiftUp = false;
		lastLift = isLiftUp;
		isReachGoal = false;
		isStopped = false;
		isSlowed = false;
		timePast = 0;
		startTime = 0;
		startTimeNano = 0;
		stateTime = 0;
		tickTime = 0;
		this.motorType = type;
	}
	
	/**
	 * Update the state of the motor
	 * @param deltaTime Float time between 2 frame
	 */
	public void update(float deltaTime) {
		// Check if the motor reached goal or not
		if ((position.x >= World.WORLD_WIDTH - 26) && !isReachGoal) {
			isReachGoal = true;
			timePastNano = System.nanoTime() - startTimeNano;
		}
		
		if (position.x >= World.WORLD_WIDTH - 20) {
			isStopped = true;
			velocity.x = 0;
			velocity.y = 0;
		}
		
		if (!isStopped) {
			if (!isSlowed) {
				if (velocity.x < MAX_VELOCITY) velocity.add(ACCEL*deltaTime, 0);
			} else {
				if (velocity.x > MIN_VELOCITY) {
					velocity.sub(ACCEL*deltaTime, 0);
				}
			}
			
			position.add(velocity.x*deltaTime, 0);
			
			// Update the bound of the motor
			if (!isLiftUp) {
//				bounds.lowerLeft.set(position.x -WIDTH/2 + 0.5f, position.y - HEIGHT/2);
				bounds.x = position.x - WIDTH/2+1f;
				bounds.y = position.y - HEIGHT/2;
			} else {
//				bounds.lowerLeft.set(position.x, position.y - HEIGHT/2);
				bounds.x = position.x;
				bounds.y = position.y - HEIGHT/2;
			}
			
			// Update the time passed for this motor
			if (!isReachGoal) {
				if (System.nanoTime() - startTime >= 1000000000.0f) {
					timePast++;
					startTime = System.nanoTime();
				}
			}
		}
		
		// Update the state time for use in animation
		stateTime += deltaTime;
	}
	
	/**
	 * Change lane for the motor
	 * @param direct Int direction to change
	 */
	public void changeLane(int direct) {
		if (direct == 1) {
			if (position.y < 5.6f) {
				position.y = isLiftUp ? 5.6f + 0.4f : 5.6f;
			}
		}
		if (direct == 2) {
			if (position.y >= 5.6f) {
				position.y = isLiftUp ? 3.6f + 0.4f : 3.6f;
			}
		}
	}
	
	/**
	 * Lift the head of the motor up
	 */
	public void lift() {
		if (lastLift != isLiftUp) {
			if (isLiftUp == true) {
				position.y += 0.4f;
				bounds.width = bounds.width / 2;
			} else {
				position.y -= 0.4f;
				bounds.width = bounds.width * 2;
			}
		}
	}
	
	/**
	 * Reduce the speed of the motor if it clash with an obstacle or other motor
	 * @param isHindered Boolean the motor clash with an obstacle or be hindered by other motor or not
	 */
	public void hit(boolean isHindered, float deltaTime) {
		tickTime += deltaTime;
		while (tickTime > TICK_HIT) {
			tickTime -= TICK_HIT;
			velocity.x = isHindered ? velocity.x/4 : velocity.x*0.8f;
		}
	}
}
