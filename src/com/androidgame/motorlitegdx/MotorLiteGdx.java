package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.Screen;
import com.androidgame.motorlitegdx.mobile.helpers.ActionResolverAndroid;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

public class MotorLiteGdx extends Game implements InputProcessor {
	boolean firstTimeCreate = true;
	boolean isMultiplay;
	
	public MotorLiteGdx(ActionResolverAndroid actionResolver, boolean isMultiplay) {
		super(actionResolver);
		this.isMultiplay = isMultiplay;
	}
	@Override
	public Screen getStartScreen () {
		if (isMultiplay) {
			return new MultiplayGameScreen(this);
		}
		return new GameScreen(this);
	}

	@Override
	public void create () {
		Settings.load();
		Assets.load();
		Gdx.input.setInputProcessor(this);
		super.create();
	}

	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean touchDown(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean touchUp(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean touchDragged(int x, int y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
