package com.androidgame.motorlitegdx;

import java.net.Socket;

import com.androidgame.motorlitegdx.helpers.OverlapTester;
import com.androidgame.motorlitegdx.helpers.Screen;
import com.androidgame.motorlitegdx.mobile.helpers.TCPConnectThread;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;


/**
 * The game playing screen
 * @author TheEmperor
 *
 */
public class MultiplayGameScreen extends Screen {
	enum GameState {
		Ready1,
		Ready2,
		Ready3,
		Running,
		Paused,
		GameOver
	}
	
	GameState state;
	OrthographicCamera cam2d;
	SpriteBatch batcher;
	
	Vector3 touchPointSingle;
	Vector3[] touchPoints;
	Rectangle replay1Bound;
	Rectangle replay2Bound;
	Rectangle quit1Bound;
	Rectangle quit2Bound;
	Rectangle resumeBound; 
	Rectangle pauseBound;
	Rectangle upBound;
	Rectangle downBound;
	Rectangle liftBound;
	Rectangle backBound;
	Rectangle forwardBound;
	Circle joystickBound;
	Circle joystickInBound;
	
	
	MultiplayWorldRenderer renderer;
	MultiplayWorld world;
	FPSLogger fpsLogger;
	float startTime;
	String overText;
	
	Socket socket;
	
	boolean buttLiftDown;
	float lastTouchX;
	float lastTouchY;
	float swipeDisX;
	float swipeDisY;
	boolean[] fingers;
	boolean isMultiTouchSupported;
	private boolean isWinner;
	private TCPConnectThread connectThread;
	private String obstacleStr;
	private String playerPos;
	private String msg;
	private String[] stateArr;
	private String[] dataArr;
	
	public MultiplayGameScreen(Game game) {
		super(game);
		
		// socket and networking connecting thread
		socket = game.actionResolver.getAndroidSocket();
		connectThread = new TCPConnectThread(socket);
		connectThread.start();
		connectThread.write("establish");
		
		getWorldData();
		
		world = new MultiplayWorld(obstacleStr, playerPos);
		state = GameState.Ready1;
		cam2d = new OrthographicCamera(800, 480);
		cam2d.position.set(400, 240, 0);
		batcher = new SpriteBatch(1000);
		renderer = new MultiplayWorldRenderer(batcher, world);
		fpsLogger = new FPSLogger();
	    startTime = System.nanoTime();
	    buttLiftDown = false;
	    
		touchPointSingle = new Vector3();
		touchPoints = new Vector3[2];
		for (int i = 0; i < 2; i++) {
			touchPoints[i] = new Vector3();
		}
	    lastTouchX = -1;
	    lastTouchY = -1;
	    fingers = new boolean[3];
	    isMultiTouchSupported = Gdx.input.isPeripheralAvailable(Peripheral.MultitouchScreen);
	    
	    // Bound of the object on the screen
		pauseBound = new Rectangle(0, 224, 64, 64);
		liftBound = new Rectangle(0, 96, 64, 64);
		upBound = new Rectangle(672, 152, 48, 32);
		downBound = new Rectangle(672, 24, 48, 32);
		backBound = new Rectangle(616, 80, 32, 48);
		forwardBound = new Rectangle(744, 80, 32, 48);
		joystickBound = new Circle(696, 104, 80);
		joystickInBound = new Circle(696, 104, 40);
		
		resumeBound = new Rectangle(352, 272, 120, 40);
		replay1Bound = new Rectangle(352, 136+8+40+24, 120, 40);
		quit1Bound = new Rectangle(352, 136+8, 120, 40);
		replay2Bound = new Rectangle(352, 256, 120, 40);
		quit2Bound = new Rectangle(352, 192, 120, 40);
	}

	private void getWorldData() {
		connectThread.write("generate");
		while (true) {
			msg = connectThread.getMessage();
			if (msg.startsWith("data")) {
				dataArr = msg.split("\\;");
				obstacleStr =  dataArr[1];
				playerPos = dataArr[2];
				return;
			}  
		}
	}

	@Override
	public void update(float deltaTime) {
		if (state == GameState.Ready1) {
	    	updateReady1();
	    }
		if (state == GameState.Ready2) {
			updateReady2();
		}
		if (state == GameState.Ready3) {
			updateReady3();
		}
	    if (state == GameState.Paused) {
	    	updatePaused();
	    }
	    if (state == GameState.Running) {
	    	updateRunning(deltaTime);
	    }
	    if (state == GameState.GameOver) {
	    	updateGameOver();
	    }
	}
	
	private void updateReady1() {
		// Only start to change stage when there is a message to play from the server
//		System.out.println("message: " + message);
		connectThread.write("play;start");
		if (connectThread.getMessage().equals("start")) {
			if (System.nanoTime() - startTime > 2000000000.0f) {
				state = GameState.Ready2;
				startTime = System.nanoTime();
			}
		}
	}
	
	private void updateReady2() {
		if (System.nanoTime() - startTime > 1000000000.0f) {
			state = GameState.Ready3;
			startTime = System.nanoTime();
		}
	}
	
	private void updateReady3() {
		if (System.nanoTime() - startTime > 1000000000.0f) {
	        state = GameState.Running;
	        world.setStartTime(System.nanoTime());
		}
	}
	private void updateRunning(float deltaTime) {
	    int direction = 0;
	    boolean[] buttLiftPointer = new boolean[3];
	    boolean buttLiftDown = false;
	    for (int i = 0; i < 3; i++) {
	    	buttLiftPointer[i] = false;
	    }
	    
	    // Game control (lift, joystick to change direction)
		for (int i = 0; i < 3; i++) {
	        if (Gdx.input.isTouched(i) == false) {
	        	fingers[i] = false;
	        	continue;
	        }
	        
        	fingers[i] = true;
			cam2d.unproject(touchPoints[i].set(Gdx.input.getX(i), Gdx.input.getY(i), 0));
			// Hold lift Button to lift the motor up
	        if (OverlapTester.pointInRectangle(liftBound, touchPoints[i].x, touchPoints[i].y)) {
	        	buttLiftPointer[i] = true;
	        } 
	        
	        // Hold and swipe the joystick to change direction
	        if (OverlapTester.pointInCircle(joystickBound, touchPoints[i].x, touchPoints[i].y)) {
	        	if (lastTouchX == -1) {
	        		if (OverlapTester.pointInCircle(joystickInBound, touchPoints[i].x, touchPoints[i].y)) {
		        		lastTouchX = touchPoints[i].x;
		        		lastTouchY = touchPoints[i].y;
		        		joystickInBound.x = touchPoints[i].x;
		        		joystickInBound.y = touchPoints[i].y;
	        		}
	        	} else {
	        		lastTouchX = touchPoints[i].x;
	        		lastTouchY = touchPoints[i].y;
	        		joystickInBound.x = touchPoints[i].x;
	        		joystickInBound.y = touchPoints[i].y;
	        		
	        		// Check if the bound of the inner joystick overlap the movement bound
	        		if (OverlapTester.overlapCircleRectangle(joystickInBound, upBound)) {
	        			direction = 1;
	        		}
	        		
	        		if (OverlapTester.overlapCircleRectangle(joystickInBound, downBound)) {
	        			direction = 2;
	        		}
	        		
	        		if (OverlapTester.overlapCircleRectangle(joystickInBound, backBound)) {
	        			world.player.isSlowed = true;
	        		} else {
	        			world.player.isSlowed = false;
	        		}
	        	}
	        }
	        
			// Check if the pause button is touched
	        if (OverlapTester.pointInRectangle(pauseBound, touchPoints[i].x, touchPoints[i].y)) {
//	            Assets.playSound(Assets.clickSound);
	        	// Send the pause signal to the server
	        	connectThread.write("pause");
	            state = GameState.Paused;
	            return;
	        }            
	        
		}
		
		// Check if there is a finger in the screen touch the lift button
		for (int i = 0; i < 3; i++) {
			buttLiftDown = buttLiftDown || buttLiftPointer[i];
		}
		world.playerLiftUp(buttLiftDown);
		
		// If nothing is touched reset the joystick state
		if (!Gdx.input.isTouched()) { 
	        	lastTouchX = -1;
	        	lastTouchY = -1;
	        	joystickInBound.x = 696;
	        	joystickInBound.y = 104;
	        	world.player.isSlowed = false;
	    }
		
        	
		// Receive opponent data from the server
		String oppState = receiveOppState();
		
		// Pause the game if received a pause signal instead
		if (oppState.equals("pause")) {
			state = GameState.Paused;
			return;
		}
		
		// Update the game
	    world.update(deltaTime, direction, oppState);
	    
	    // Send the player's state to the server
	    sendPlayerState();
	    
	    // If the game is over
	    if (world.isGameOver) {
	    	state = GameState.GameOver;
	    	if (world.player.place == 1) {
	    		overText = world.timePastToString() + ". You Won!";
	    		isWinner = true;
	    	} else {
	    		overText = world.timePastToString() + ". You Lose!";
	    		isWinner = false;
	    	}
	    }
	}
	
	/**
	 * Receive the state string from the server through TCP connection thread
	 * @return opponent's state string
	 */
	private String receiveOppState() {
		msg = connectThread.getMessage();
		if (msg.startsWith("state")) {
			stateArr = msg.split("\\;");
			return stateArr[1];
		} else if (msg.equals("pause")) {
			// If the server return a pause signal return pause
			return "pause";
		}
		
		return "";
	}

	/**
	 * Send the player'state to the server under the format of a string
	 */
	private void sendPlayerState() {
		int isLift = 0;
		int isReachGoal = 0;
		int isStopped = 0;
		if (world.player.isLiftUp) {
			isLift = 1;
		}
		if (world.player.isReachGoal) {
			isReachGoal = 1;
		}
		if (world.player.isStopped) {
			isStopped = 1;
		}
		String msg = "state;" + world.player.position.x + ":" + world.player.position.y + ":" + isLift + ":" 
					+ isReachGoal + ":" + isStopped;
		connectThread.write(msg);
	}

	private void updatePaused() {
		// If a resume signal received from the server
		if (connectThread.getMessage().equals("resume")) {
			System.out.println("Resume is comming");
			state = GameState.Running;
			return;
		}
		
		if (Gdx.input.justTouched()) {
			cam2d.unproject(touchPointSingle.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			
			// If the resume button was touched
			if (OverlapTester.pointInRectangle(resumeBound, touchPointSingle.x, touchPointSingle.y)) {
//	            Assets.playSound(Assets.clickSound);
				// Send the resume signal to the server
				connectThread.write("resume");
	            state = GameState.Running;
	            return;
	        }
	        
			// If the replay button was touched
	        if (OverlapTester.pointInRectangle(replay1Bound, touchPointSingle.x, touchPointSingle.y)) {
//	            Assets.playSound(Assets.clickSound);
	            game.setScreen(new GameScreen(game));
	            return;
	        }
	        
	        // If the quit button was touched
	        if (OverlapTester.pointInRectangle(quit1Bound, touchPointSingle.x, touchPointSingle.y)) {
	        	game.actionResolver.quitGDXGame(false);
	        	connectThread.cancel();
	        	game.dispose();
	        }
		}
	}
	
	private void updateGameOver() {
		if (Gdx.input.justTouched()) {
			cam2d.unproject(touchPointSingle.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			
			// IF the resume button was touched
	        if (OverlapTester.pointInRectangle(replay2Bound, touchPointSingle.x, touchPointSingle.y)) {
//	            Assets.playSound(Assets.clickSound);
	            game.setScreen(new GameScreen(game));
	            return;
	        }
	        
	        // If the quit button was touched
	        if (OverlapTester.pointInRectangle(quit2Bound, touchPointSingle.x, touchPointSingle.y)) {
	        	game.actionResolver.saveUserScore(isWinner, Float.parseFloat(world.timePastToString()));
	        	game.actionResolver.quitGDXGame(true);
	        	connectThread.cancel();
	        	game.dispose();
	        }
		}
	}

	@Override
	public void present(float deltaTime) {
		GLCommon gl = Gdx.gl;
	    gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	    gl.glEnable(GL10.GL_TEXTURE_2D);
	    
	    renderer.render();
	    
	    cam2d.update();
	    batcher.setProjectionMatrix(cam2d.combined);
	    batcher.enableBlending();
	    batcher.begin();
	    if (state == GameState.Ready1) {
	    	presentReady(0);
	    }
	    if (state == GameState.Ready2) {
	    	presentReady(1);
	    }
	    if (state == GameState.Ready3) {
	    	presentReady(2);
	    }
	    if (state == GameState.Paused) {
	    	presentPaused();
	    }
	    if (state == GameState.Running) {
	    	presentRunning();
	    }
	    if (state == GameState.GameOver) {
	    	presentGameOver();
	    }
	    batcher.end();
	    
	    if (state == GameState.GameOver) {
	    	presentOverText();
	    }
	    
	    if (state == GameState.Running) {
	    	presentStatus();
	    }
	    
	    gl.glDisable(GL10.GL_BLEND);
	    fpsLogger.log();
	}
	
	private void presentReady(int ready) {
		batcher.draw(Assets.readyMenu[2-ready], 344, 136, 144, 288);
	}
	
	private void presentRunning() {
		batcher.draw(Assets.pausedButton, 0, 224, 64, 64);
		batcher.draw(Assets.liftButton, 0, 96, 64, 64);
		batcher.draw(Assets.joystickOut, 600, 8, 192, 192);
		if (lastTouchX == -1) {
			batcher.draw(Assets.joystickIn, 656, 64, 80, 80);
		} else {
			float tempX = lastTouchX;
			float tempY = lastTouchY;
			if (lastTouchX > 696 + 40) {
				tempX = 696 + 40;
			}
			if (lastTouchX < 696 - 40) {
				tempX = 696 - 40;
			}
			if (lastTouchY > 104 + 40) {
				tempY = 104 + 40;
			}
			if (lastTouchY < 104 - 40) {
				tempY = 104 - 40;
			}
			batcher.draw(Assets.joystickIn, tempX-40, tempY-40, 80, 80);
		}
		
		// Draw the output for multitouch
//		Assets.textFont.setScale(1.2f);
//		Assets.textFont.draw(batcher, "Multitouch Support: " + isMultiTouchSupported, 520, 430);
//		for (int i = 0; i < 3; i++) {
//			Assets.textFont.draw(batcher, "Finger " +i+ " down: " + fingers[i], 520, 400-i*30);
//		}
	}
	
	private void presentStatus() {
		batcher.begin();
//		Assets.speedFont.drawText(batcher, String.format("%.1f",5*world.player.velocity.x)+" KM /H", 20, 480-24, 18);
		Assets.speedFont.draw(batcher, String.format("%.1f",5*world.player.velocity.x), 8 , 480-8);
		Assets.speedFont.draw(batcher, "Km/H", 5, 480-36);
		batcher.end();
		
		batcher.begin();
		batcher.draw(Assets.minimap, 480 - 320, 480 - 44, 640, 44);
		presentMapMotors();
		batcher.end();
	}
	
	private void presentMapMotors() {
		float playerMapX = 160 + world.player.position.x/52 * 32;
		float playerMapY = world.player.position.y >= 4.6f ? 480 - 11 : 480 - 33;
		float oppMapX = 160 + world.opponent.position.x/52 * 32;
		float oppMapY = world.opponent.position.y >= 4.6f ? 480 - 11 : 480 - 33;
		batcher.draw(Assets.mapPlayer, playerMapX - 5, playerMapY - 5, 10, 10);
		batcher.draw(Assets.mapAI, oppMapX - 5, oppMapY - 5, 10, 10);
	}
	
	private void presentPaused() {
		batcher.draw(Assets.pausedMenu, 344, 136, 144, 288);
	}
	
	private void presentGameOver() {
		batcher.draw(Assets.gameOverMenu, 344, 136, 144, 288);
	}
	
	private void presentOverText() {
		batcher.begin();
		Assets.textFont.setColor(Color.RED);
		Assets.textFont.draw(batcher, overText, 344, 136+60);
		batcher.end();
	}

	@Override
	public void pause() {
		if (state == GameState.Running) state = GameState.Paused;
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}