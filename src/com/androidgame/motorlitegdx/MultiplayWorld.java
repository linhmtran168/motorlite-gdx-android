package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.OverlapTester;

public class MultiplayWorld extends World {

	public Motor opponent;
	
	public MultiplayWorld(String obstacleStr, String playerPos) {
		super(true);
		if (Integer.parseInt(playerPos) == 0) {
			this.player = new Motor(5, 3.6f, 0);
			this.opponent = new Motor(5, 5.6f, 1);
		} else {
			this.player = new Motor(5, 5.6f, 1);
			this.opponent = new Motor(5, 3.6f, 0);
		}
		generateRoad(obstacleStr);
	}
	
	/**
	 * Add obstacle and milestone to the world
	 */
	public void generateRoad(String obstacleStr) {
		// Generate obstacle
		String[] obstacleArr = obstacleStr.split("\\|");
		
		for (int i = 0; i < obstacleArr.length; i++) {
			String[] obstacleInfo = obstacleArr[i].split("\\:");
			int type = Integer.parseInt(obstacleInfo[2]);
			float x = Float.parseFloat(obstacleInfo[0]);
			float y = Float.parseFloat(obstacleInfo[1]);
			
			if (type == 0) {
				Obstacle obstacle = new Obstacle.Brick(x, y);
				obstacles.add(obstacle);
			} else if (type == 1) {
				Obstacle obstacle = new Obstacle.Can(x, y);
				obstacles.add(obstacle);
			} else {
				Obstacle obstacle = new Obstacle.Nails(x, y);
				obstacles.add(obstacle);
			}
		}
		
		// Generate milestone
		int y = 50;
		while (y < WORLD_WIDTH - 26) {
			Milestone milestone = new Milestone(y, 5.5f);
			milestone.miles = (20 - y/50) + "";
			milestones.add(milestone);
			y += 50;
		}
	}
	
	
	/**
	 * Update the game world
	 * @param deltaTime float time between 2 frame
	 * @param direction int direction to change lange
	 * @param buttLiftDown boolean the status of the Lift button
	 */
	public void update(float deltaTime, int direction, String oppState) {
		checkGameOver();
		updateMotor(deltaTime, direction, oppState);
		checkClash(deltaTime);
	}
	
	/**
	 * Update the state of the motors
	 * @param deltaTime
	 * @param direction
	 * @param buttLiftDown
	 */
	private void updateMotor(float deltaTime, int direction, String oppState) {
		// Change lane after the direction
		if (direction != 0) {
			player.changeLane(direction);
		}
		
		player.update(deltaTime);
		if (!oppState.equals("")) {
			updateOpponent(oppState);
		}
		updatePlace();
	}
	
	/**
	 * Extract the string receive from the server and update the opponent state
	 * @param oppState the state string
	 */
	private void updateOpponent(String oppState) {
		String[] stateArr = oppState.split("\\:");
		opponent.position.x = Float.parseFloat(stateArr[0]);
		opponent.position.y = Float.parseFloat(stateArr[1]);
		opponent.isLiftUp = Integer.parseInt(stateArr[2]) == 1 ? true : false;
		opponent.isReachGoal = Integer.parseInt(stateArr[3]) == 1 ? true : false;
		opponent.isStopped = Integer.parseInt(stateArr[4]) == 1 ? true : false;
	}
	
	/**
	 * If the lift button pressed, change player's state
	 * @param buttLiftDown boolean lift button is pressed or not
	 */
	public void playerLiftUp(boolean buttLiftDown) {
		// If lift up button is down, lift the motor
		if (buttLiftDown) {
//			System.out.println("World Lift: " + buttLiftDown);
			player.lastLift = player.isLiftUp;
			player.isLiftUp = true;
			player.lift();
		} else {
			if (player.isLiftUp) {
				player.lastLift = player.isLiftUp;
				player.isLiftUp = false;
				player.lift();
			}
		}
		
	}
	
	/**
	 * Update the position of players
	 */
	private void updatePlace() {
		if (player.isReachGoal && opponent.isReachGoal) {
			if (player.timePastNano == opponent.timePastNano) {
				player.place = opponent.place = 1;
			} else if (player.timePastNano > opponent.timePastNano) {
				player.place = 2;
				opponent.place = 1;
			} else {
				player.place = 1;
				opponent.place = 2;
			}
		} else {
			if (opponent.position.x < player.position.x) {
				opponent.place = 2;
				player.place = 1;
			} else if (opponent.position.x > player.position.x) {
				player.place = 2;
				opponent.place = 1;
			} else {
				player.place = opponent.place = 1;
			}
		}
	}
	
	/**
	 * Check clash between motor and motor, motor and obstacles
	 */
	private void checkClash(float deltaTime) {
		checkClashObstacles(deltaTime);
		checkClashMotors(deltaTime);
	}
	
	/**
	 * Check if a motor clash with an obstacle or not
	 */
	private void checkClashObstacles(float deltaTime) {
		int len = obstacles.size();
		
		for (int i = 0; i < len; i++) {
			Obstacle obstacle = obstacles.get(i);
			float dist = obstacle.position.x - player.position.x;
			
			if (dist >= -4.5f && dist <= 6) {
				if (OverlapTester.overlapRectangles(player.bounds, obstacle.bounds)) {
					player.hit(true, deltaTime);
				}
			}
			
		}
	}
	
	private void checkClashMotors(float deltaTime) {
		float gapX = player.position.x - opponent.position.x;
		
//		if (gapX >= -4.5f && gapX <= 4.5f) {
//			if (OverlapTester.overlapRectangles(player.bounds, opponent.bounds)) {
//				// The motor that has larger X co-ordinate will be less affected by the clash
//				if (opponent.position.x != player.position.x) {
//					if (opponent.position.x > player.position.x) {
//						player.hit(true, deltaTime);
//						opponent.hit(false, deltaTime);
//					} else {
//						player.hit(false, deltaTime);
//						opponent.hit(true, deltaTime);
//					}
//				} else {
//					if (rand.nextInt(2) == 0) {
//						player.hit(true, deltaTime);
//						opponent.hit(false, deltaTime);
//					} else {
//						player.hit(false, deltaTime);
//						opponent.hit(true, deltaTime);
//					}
//				}
//			}
//		}
	}
	
	/**
	 * Check game over
	 */
	private void checkGameOver() {
		if (player.isStopped) {
			isGameOver = true;
		}
	}
	
	/**
	 * Check if the player and obstacles are on the same lane or not
	 * @param motorY
	 * @param obstacleY
	 * @return
	 */
	private boolean isSameLane(float motorY, float obstacleY) {
		float gap = motorY - obstacleY;
		if (gap < 1 || gap > 3) {
			return false;
		}
		return true;
	}
	
	/**
	 * Set the start time for the motors
	 * @param time 
	 */
	public void setStartTime(float time) {
		player.startTime = time;
		opponent.startTime = time;
		player.startTimeNano = time;
		opponent.startTimeNano = time;
	}
	
	/**
	 * Change the time past to 1.00 formating
	 * @return String time passed in string.
	 */
	public String timePastToString() {
		int minute = (player.timePast < 0 ? 0 : player.timePast / 60);
		int second = (player.timePast < 0 ? 0 : (player.timePast % 60));
		String minute_s = "" + minute;
		String second_s = second < 10 ? "0" + second : "" + second;
		String time = minute_s + "." + second_s;
		return time;
	}
}
