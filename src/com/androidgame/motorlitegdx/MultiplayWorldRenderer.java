package com.androidgame.motorlitegdx;

import com.androidgame.motorlitegdx.helpers.Animation;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MultiplayWorldRenderer implements Renderer {

	OrthographicCamera cam2d;
	MultiplayWorld world;
	SpriteBatch batcher;
	boolean isRendered;
	
	/**
	 * The class constructor
	 * @param batcher SpriteBatch the spritebatch to render texture
	 * @param world World an object of the world simulation class
	 */
	public MultiplayWorldRenderer(SpriteBatch batcher, MultiplayWorld world) {
		this.batcher = batcher;
		this.world = world;
		this.cam2d = new OrthographicCamera(FRUSTUM_WIDTH, FRUSTUM_HEIGHT);
		this.cam2d.position.set(FRUSTUM_WIDTH/2, FRUSTUM_HEIGHT/2, 0);
		this.isRendered = false;
	}
	
	public void render() {
		cam2d.position.x = world.player.position.x + FRUSTUM_WIDTH/2 - 5;
		cam2d.update();
		batcher.setProjectionMatrix(cam2d.combined);
		renderBackGround();
		renderObstacles();
		renderMotors();
	}
	
	private void renderBackGround() {
		// Render the background scene
		int partNext = 0;
		for(int i = 0; i <= World.WORLD_WIDTH; i += 32) {
			batcher.begin();
//			TextureRegion mapRegion = new TextureRegion(Assets.maps[partNext], 0, 0, 1024, 480);
			batcher.draw(Assets.mapRegions[partNext], i, 0, 32, 15);
			batcher.end();
			if (partNext == 4) {
				partNext = 0;
			} else {
				partNext++;
			}
		}
		
		// Render the goal flag
		batcher.begin();
		batcher.draw(Assets.flag, World.WORLD_WIDTH - 26 - 6.5f , 0.2f, 13, 13);
		batcher.end();
		
		// Render the milestones
		batcher.begin();
		int len = world.milestones.size();
		for (int i = 0; i < len; i++) {
			Milestone milestone = world.milestones.get(i);
			batcher.draw(Assets.milestone, milestone.position.x - Milestone.WIDTH/2, milestone.position.y - Milestone.HEIGHT/2, Milestone.WIDTH, Milestone.HEIGHT);
		}
		batcher.end();
		
		// Render the milestones number
		batcher.begin();
//		Log.d("font Size", Assets.fontTex.width+"");
		for (int i = 0; i < len; i++) {
			Milestone milestone = world.milestones.get(i);
			float posX = milestone.miles.length() > 1 ? milestone.position.x - 0.4f : milestone.position.x-0.2f;
			Assets.milestoneNumber.drawText(batcher, milestone.miles, posX, milestone.position.y - 0.65f, 32, 0);
		}
		batcher.end();
	}
	
	private void renderMotors() {
        batcher.begin();
        if (world.opponent.position.y < world.player.position.y) {
        	renderAnimatedMotor(world.player);
        	renderAnimatedMotor(world.opponent);
        } else {
        	renderAnimatedMotor(world.opponent);
        	renderAnimatedMotor(world.player);
        }
		batcher.end();
	}
	
	private void renderAnimatedMotor(Motor motor) {
		TextureRegion keyFrame;
		if (motor.motorType == 0) {
			keyFrame = Assets.motor.getKeyFrame(motor.stateTime, Animation.ANIMATION_LOOPING);
		} else {
			keyFrame = Assets.aiMotor.getKeyFrame(motor.stateTime, Animation.ANIMATION_LOOPING);
		}
		if (!motor.isLiftUp) {
			batcher.draw(keyFrame, motor.position.x - Motor.WIDTH/2, motor.position.y-Motor.HEIGHT/2, Motor.WIDTH, Motor.HEIGHT);
		} else {
			batcher.draw(keyFrame, motor.position.x - Motor.WIDTH/2, motor.position.y-Motor.HEIGHT/2, Motor.WIDTH/2, Motor.HEIGHT/2, Motor.WIDTH, Motor.HEIGHT, 1, 1, 45);
		}
	}
	
	private void renderObstacles() {
		batcher.begin();
		int len = world.obstacles.size();
		for (int i = 0; i < len; i++) {
			Obstacle obstacle = world.obstacles.get(i);
			switch(obstacle.type) {
			case 0:
				batcher.draw(Assets.brick, obstacle.position.x-Obstacle.BRICK_WIDTH/2, obstacle.position.y-Obstacle.BRICK_HEIGHT/2, Obstacle.BRICK_WIDTH, Obstacle.BRICK_HEIGHT);
				break;
			case 1:
				batcher.draw(Assets.can, obstacle.position.x-Obstacle.BRICK_WIDTH/2, obstacle.position.y-Obstacle.BRICK_HEIGHT/2, Obstacle.CAN_WIDTH, Obstacle.CAN_HEIGHT);
				break;
			case 2:	
				batcher.draw(Assets.nails, obstacle.position.x-Obstacle.BRICK_WIDTH/2, obstacle.position.y-Obstacle.BRICK_HEIGHT/2, Obstacle.NAILS_WIDTH, Obstacle.NAILS_HEIGHT);
				break;
			}
		}
		batcher.end();
	}
}
