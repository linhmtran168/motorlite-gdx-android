package com.androidgame.motorlitegdx;

public interface Renderer {
	static final float FRUSTUM_WIDTH = 25;
	static final float FRUSTUM_HEIGHT = 15;
	
	public void render();
}
