package com.androidgame.motorlitegdx;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.androidgame.motorlitegdx.helpers.OverlapTester;

/**
 * The simulation class for the game world
 */
public class World {
	public static final float TICK_AI = 1f;
	public static final float TICK_CLASH = 0.1f;
	public static final float WORLD_WIDTH = 1040;
	public static final float WORLD_HEIGHT = 15;
	
	public Motor player;
	public AIMotor aiPlayer;
	public final List<Obstacle> obstacles;
	public final List<Milestone> milestones;
	public final Random rand;
	
	public boolean isGameOver;
	
	public World() {
		this.player = new Motor(5, 3.6f, 0);
		this.aiPlayer = new AIMotor(5, 5.6f, 1);
		this.obstacles = new ArrayList<Obstacle>();
		this.milestones = new ArrayList<Milestone>();
		rand = new Random();
		isGameOver = false;
		generateRoad();
	}
	
	public World(boolean isMultiplay) {
		rand = new Random();
		isGameOver = false;
		obstacles = new ArrayList<Obstacle>();
		milestones = new ArrayList<Milestone>();
	}
	
	/**
	 * Add obstacle and milestone to the world
	 */
	public void generateRoad() {
		int x = 25;
		int y = 50;
		while (x < WORLD_WIDTH - 36) {
			addObstacle(x, rand.nextInt(3));
			x += 6+rand.nextInt(20);
		}
		while (y < WORLD_WIDTH - 26) {
			Milestone milestone = new Milestone(y, 5.5f);
			milestone.miles = (20 - y/50) + "";
			milestones.add(milestone);
			y += 50;
		}
	}
	
	/**
	 * Add obstacle to the world
	 * @param x Float x coordinate of the obstacle
	 * @param type Int type of the obstacle (image to display)
	 */
	private void addObstacle(float x, int type) {
		float y_random = rand.nextInt(2) == 0 ? 1.5f : 3.7f;
		if (type == 0) {
			Obstacle obstacle = new Obstacle.Brick(x, y_random);
			obstacles.add(obstacle);
		} else if (type == 1) {
			Obstacle obstacle = new Obstacle.Can(x, y_random);
			obstacles.add(obstacle);
		} else {
			Obstacle obstacle = new Obstacle.Nails(x, y_random);
			obstacles.add(obstacle);
		}
	}
	
	/**
	 * Update the game world
	 * @param deltaTime float time between 2 frame
	 * @param direction int direction to change lange
	 * @param buttLiftDown boolean the status of the Lift button
	 */
	public void update(float deltaTime, int direction) {
		checkGameOver();
		updateMotor(deltaTime, direction);
		checkClash(deltaTime);
	}
	
	/**
	 * Update the state of the motors
	 * @param deltaTime
	 * @param direction
	 * @param buttLiftDown
	 */
	private void updateMotor(float deltaTime, int direction) {
		// Change lane after the direction
		if (direction != 0) {
			player.changeLane(direction);
		}
		
		updatePlace();
		player.update(deltaTime);
		aiPlayer.update(deltaTime, player);
	}
	
	public void playerLiftUp(boolean buttLiftDown) {
		// If lift up button is down, lift the motor
		if (buttLiftDown) {
//			System.out.println("World Lift: " + buttLiftDown);
			player.lastLift = player.isLiftUp;
			player.isLiftUp = true;
			player.lift();
		} else {
			if (player.isLiftUp) {
				player.lastLift = player.isLiftUp;
				player.isLiftUp = false;
				player.lift();
			}
		}
		
	}
	
	/**
	 * Update the position of players
	 */
	private void updatePlace() {
		if (player.isReachGoal && aiPlayer.isReachGoal) {
			if (player.timePastNano == aiPlayer.timePastNano) {
				player.place = aiPlayer.place = 1;
			} else if (player.timePastNano > aiPlayer.timePastNano) {
				player.place = 2;
				aiPlayer.place = 1;
			} else {
				player.place = 1;
				aiPlayer.place = 2;
			}
		} else {
			if (aiPlayer.position.x < player.position.x) {
				aiPlayer.place = 2;
				player.place = 1;
			} else if (aiPlayer.position.x > player.position.x) {
				player.place = 2;
				aiPlayer.place = 1;
			} else {
				player.place = aiPlayer.place = 1;
			}
		}
	}
	
	/**
	 * Check clash between motor and motor, motor and obstacles
	 */
	private void checkClash(float deltaTime) {
		checkClashObstacles(deltaTime);
		checkClashMotors(deltaTime);
	}
	
	/**
	 * Check if a motor clash with an obstacle or not
	 */
	private void checkClashObstacles(float deltaTime) {
		int len = obstacles.size();
		
		for (int i = 0; i < len; i++) {
			Obstacle obstacle = obstacles.get(i);
			float dist = obstacle.position.x - player.position.x;
			float dist2 = obstacle.position.x - aiPlayer.position.x;
			
			if (dist >= -4.5f && dist <= 6) {
				if (OverlapTester.overlapRectangles(player.bounds, obstacle.bounds)) {
					player.hit(true, deltaTime);
				}
			}
			
			// Make the AI evade the obstacle
			if (dist2 >= -4.5f && dist2 <= 6) {
				if (dist2 <= 6 && isSameLane(aiPlayer.position.y, obstacle.position.y)) {
					if (obstacle.position.y == 1.5f) {
						aiPlayer.evade(1);
					} else {
						aiPlayer.evade(2);
					}
				}
				if (OverlapTester.overlapRectangles(aiPlayer.bounds, obstacle.bounds)) {
					aiPlayer.hit(true, deltaTime);
				}
			}
		}
	}
	
	private void checkClashMotors(float deltaTime) {
		float gapX = player.position.x - aiPlayer.position.x;
		float gapY = player.position.y - aiPlayer.position.y;
		
		// Make the AI can auto evade the player
		if (gapX > 0 && gapX <= 6) {
			if (gapY == 0 || gapY == 0.4f || gapY == -0.4f) {
				if (aiPlayer.position.y >= 5.6) {
					aiPlayer.evade(2);
				} else {
					aiPlayer.evade(1);
				}
			}
		
		}
		
		// Make the AI can auto hinder the player
		if (gapX >= -4.5f && gapX <= 4.5f) {
			if (OverlapTester.overlapRectangles(player.bounds, aiPlayer.bounds)) {
				// The motor that has larger X co-ordinate will be less affected by the clash
				if (aiPlayer.position.x != player.position.x) {
					if (aiPlayer.position.x > player.position.x) {
						player.hit(true, deltaTime);
						aiPlayer.hit(false, deltaTime);
					} else {
						player.hit(false, deltaTime);
						aiPlayer.hit(true, deltaTime);
					}
				} else {
					if (rand.nextInt(2) == 0) {
						player.hit(true, deltaTime);
						aiPlayer.hit(false, deltaTime);
					} else {
						player.hit(false, deltaTime);
						aiPlayer.hit(true, deltaTime);
					}
				}
			}
		}
	}
	
	/**
	 * Check game over
	 */
	private void checkGameOver() {
		if (player.isStopped) {
			isGameOver = true;
		}
	}
	
	/**
	 * Check if the player and obstacles are on the same lane or not
	 * @param motorY
	 * @param obstacleY
	 * @return
	 */
	private boolean isSameLane(float motorY, float obstacleY) {
		float gap = motorY - obstacleY;
		if (gap < 1 || gap > 3) {
			return false;
		}
		return true;
	}
	
	/**
	 * Set the start time for the motors
	 * @param time 
	 */
	public void setStartTime(float time) {
		player.startTime = time;
		aiPlayer.startTime = time;
		player.startTimeNano = time;
		aiPlayer.startTimeNano = time;
	}
	
	/**
	 * Change the time past to 1.00 formating
	 * @return String time passed in string.
	 */
	public String timePastToString() {
		int minute = (player.timePast < 0 ? 0 : player.timePast / 60);
		int second = (player.timePast < 0 ? 0 : (player.timePast % 60));
		String minute_s = "" + minute;
		String second_s = second < 10 ? "0" + second : "" + second;
		String time = minute_s + "." + second_s;
		return time;
	}
}
