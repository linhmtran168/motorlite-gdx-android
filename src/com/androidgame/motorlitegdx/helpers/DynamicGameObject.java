package com.androidgame.motorlitegdx.helpers;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class DynamicGameObject extends GameObject {
	public Vector2 velocity;
	public Vector2 accel;

	public DynamicGameObject(float x, float y, float width, float height) {
		super(x, y, width, height);
		velocity = new Vector2();
		accel = new Vector2();
	}
	
	public DynamicGameObject(float x, float y, Rectangle bounds) {
		super(x, y, bounds);
		velocity = new Vector2();
		accel = new Vector2();
	}
}
