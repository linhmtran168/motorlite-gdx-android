package com.androidgame.motorlitegdx.helpers;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * The class contains methods to check the oveverlap of objects
 * @author TheEmperor
 *
 */
public class OverlapTester {
	public static boolean overlapRectangles (Rectangle r1, Rectangle r2) {
		if (r1.x < r2.x + r2.width && r1.x + r1.width > r2.x && r1.y < r2.y + r2.height && r1.y + r1.height > r2.y)
			return true;
		else
			return false;
	}

	public static boolean pointInRectangle (Rectangle r, Vector2 p) {
		return r.x <= p.x && r.x + r.width >= p.x && r.y <= p.y && r.y + r.height >= p.y;
	}

	public static boolean pointInRectangle (Rectangle r, float x, float y) {
		return r.x <= x && r.x + r.width >= x && r.y <= y && r.y + r.height >= y;
	}
	
	public static boolean overlapCircleRectangle(Circle c, Rectangle r) {
        float closestX = c.x;
        float closestY = c.y;
        Vector2 center = new Vector2(c.x, c.y);
        
        if(c.x < r.x) {
            closestX = r.x; 
        } 
        else if(c.x > r.x + r.width) {
            closestX = r.x + r.width;
        }
          
        if(c.y < r.y) {
            closestY = r.y;
        } 
        else if(c.y > r.y + r.height) {
            closestY = r.y + r.height;
        }
        
        return center.dst2(new Vector2(closestX, closestY)) < c.radius * c.radius;           
    }
	
    public static boolean pointInCircle(Circle c, Vector2 p) {
    	Vector2 center = new Vector2(c.x, c.y);
        return center.dst2(p) < c.radius * c.radius;
    }
    
    public static boolean pointInCircle(Circle c, float x, float y) {
    	Vector2 center = new Vector2(c.x, c.y);
    	Vector2 point = new Vector2(x, y);
        return center.dst2(point) < c.radius * c.radius;
    }
}
