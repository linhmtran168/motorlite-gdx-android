package com.androidgame.motorlitegdx.helpers;

import com.androidgame.motorlitegdx.Game;

/**
 * The screen helper abstract class
 * @author TheEmperor
 *
 */
public abstract class Screen {
	protected Game game;

	public Screen (Game game) {
		this.game = game;
	}

	public abstract void update (float deltaTime);

	public abstract void present (float deltaTime);

	public abstract void pause ();

	public abstract void resume ();

	public abstract void dispose ();
}
