package com.androidgame.motorlitegdx.helpers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureFont {
    public final Texture texture;
    public final float glyphWidth;
    public final float glyphHeight;
    public final TextureRegion[] glyphs = new TextureRegion[96];   
    
    public TextureFont(Texture texture, 
                int offsetX, int offsetY,
                int glyphsPerRow, int glyphWidth, int glyphHeight) {        
        this.texture = texture;
        this.glyphWidth = glyphWidth;
        this.glyphHeight = glyphHeight;
        int x = offsetX;
        int y = offsetY;
        for(int i = 0; i < 96; i++) {
            glyphs[i] = new TextureRegion(texture, x, y, glyphWidth, glyphHeight);
            x += glyphWidth;
            if(x == offsetX + glyphsPerRow * glyphWidth) {
                x = offsetX;
                y += glyphHeight;
            }
        }        
    }
    
    public void drawText(SpriteBatch batcher, String text, float x, float y) {
        int len = text.length();
        for(int i = 0; i < len; i++) {
            int c = text.charAt(i) - ' ';
            if(c < 0 || c > glyphs.length - 1) 
                continue;
            
            TextureRegion glyph = glyphs[c];
            batcher.draw(glyph, x, y, glyphWidth, glyphHeight);
            x += glyphWidth;
        }
    }
    
    public void drawText(SpriteBatch batcher, String text, float x, float y, float gap) {
        int len = text.length();
        for(int i = 0; i < len; i++) {
            int c = text.charAt(i) - ' ';
            if(c < 0 || c > glyphs.length - 1) 
                continue;
            
            TextureRegion glyph = glyphs[c];
            batcher.draw(glyph, x, y, glyphWidth, glyphHeight);
            x += glyphWidth - gap;
        }
    }
    
    public void drawText(SpriteBatch batcher, String text, float x, float y, float scale, float gap) {
        int len = text.length();
        for(int i = 0; i < len; i++) {
            int c = text.charAt(i) - ' ';
            if(c < 0 || c > glyphs.length - 1) 
                continue;
            
            TextureRegion glyph = glyphs[c];
            batcher.draw(glyph, x, y, glyphWidth/scale, glyphHeight/scale);
            x += glyphWidth/scale - gap;
        }
    }
}
