package com.androidgame.motorlitegdx.mobile;

import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainMenuActivity extends Activity {

	private TextView welcomeText;
	private Button instructButton;
	private Button statButton;
	private Button startButton;
	private Button changeProfileButton;
	private Button multiplayButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		setUpViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		String profileName = ((MotorLiteApplication)getApplication()).getCurrentUser().getProfileName();
		welcomeText.setText("Welcome, "+profileName);
	}

	private void setUpViews() {
		welcomeText = (TextView)findViewById(R.id.welcome_text);
		startButton = (Button)findViewById(R.id.start_button);
		statButton = (Button)findViewById(R.id.statistic_button);
		instructButton = (Button)findViewById(R.id.intrucstion_button);
		changeProfileButton = (Button)findViewById(R.id.change_profile_button);
		multiplayButton = (Button)findViewById(R.id.multi_button);
		
		startButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, MotorLiteAndroidActivity.class);
				startActivity(intent);
				MainMenuActivity.this.finish();
			}
		});
		
		multiplayButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, MultiplayStartActivity.class);
				startActivity(intent);
				MainMenuActivity.this.finish();
			}
		});
		
		statButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, StatisticActivity.class);
				startActivity(intent);
			}
		});
		
		instructButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, InstructionActivity.class);
				startActivity(intent);
			}
		});
		
		changeProfileButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, ViewProfilesActivity.class);
				startActivity(intent);
			}
		});
	}
}
