package com.androidgame.motorlitegdx.mobile;

import android.os.Bundle;

import com.androidgame.motorlitegdx.MotorLiteGdx;
import com.androidgame.motorlitegdx.mobile.helpers.ActionResolverAndroid;
import com.badlogic.gdx.backends.android.AndroidApplication;

public class MotorLiteAndroidActivity extends AndroidApplication {
    private ActionResolverAndroid actionResolver;

	/** Called when the activity is first created. */
    @Override
    public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		actionResolver = new ActionResolverAndroid(this);
		initialize(new MotorLiteGdx(actionResolver, false), false);
	}
}