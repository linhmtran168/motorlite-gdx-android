package com.androidgame.motorlitegdx.mobile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.adapter.OnlinePlayerAdapter;
import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;
import com.androidgame.motorlitegdx.mobile.helpers.RequestMethod;
import com.androidgame.motorlitegdx.mobile.helpers.RestClient;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class MultiplayStartActivity extends ListActivity {
	
	private static final String TCP_IP = "203.162.16.44";
	private static final String TCP_PORT = "8088";
	
	private Button mainMenuButton;
	private TextView helloText;
	private TextView noteText;
	private MotorLiteApplication app;
	private UserProfile currentUser;
	private ProgressDialog progressDialog;
	private ProgressDialog waitingDialog;
	private AlertDialog problemTextDialog;
	private AlertDialog connectTextDialog;
	private AlertDialog replyDialog;
	private AlertDialog busyDialog;
	private AlertDialog readyWaitingDialog;
	private AlertDialog cancelDialog;
	private Socket socket;
	private ArrayList<JSONObject> playersList;
	private OnlinePlayerAdapter adapter;
	private ConnectThread connectThread;
	
	/**
	 * Nested class to implement the socket connection thread
	 * @author TheEmperor
	 *
	 */
	private class ConnectThread extends Thread {
		private final Socket socket;
		private final BufferedReader reader;
		private final BufferedWriter writer;
		
		public ConnectThread(Socket sock) {
			socket = sock;
			BufferedReader tmpReader = null;
			BufferedWriter tmpWriter = null;
			
			try {
				tmpReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				tmpWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			reader = tmpReader;
			writer = tmpWriter;
		}
		
		/**
		 * Keep listening to the InputStream to receive message
		 * until an exception occurs
		 */
		public void run() {
			String line = "";
			
			if (Thread.currentThread().isInterrupted()) {
				try {
					reader.close();
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			while (!Thread.currentThread().isInterrupted()) {
				try {
					// Read line from the input stream
					line = reader.readLine();
//					Log.d("response", line);
					// Update the activity UI
					uiUpdater.obtainMessage(1, line).sendToTarget();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		public void write(String line) {
			try {
				// Write line to the TCP server
				writer.write(line+"\n", 0, line.length() + 1);
				writer.flush();
				Log.d("write line", line);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	
		public void cancel() {
			try {
				// Stop the listening loop
				// Close the socket 
				socket.close();
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Nested class to connect to the rest API in the background
	 * @author TheEmperor
	 *
	 */
	private class InitialConnectTask extends AsyncTask<String, Void, Integer> {
		
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Getting your online id");
			progressDialog.show();
		}
		
		@Override
		protected Integer doInBackground(String... params) {
			// Create a post request to the HTTP server
			RestClient client = new RestClient(params[0]);
			client.addHeader("Authorization", params[1]);
			client.addParam("name", currentUser.getProfileName());
			client.addParam("win", currentUser.getWin().toString());
			client.addParam("lose", currentUser.getLose().toString());
			client.addParam("best_time", currentUser.getBestTime().toString());
			try {
				client.execute(RequestMethod.POST);
				if (client.getResponseCode() != 200) {
					return null;
				} else {
					// Return the id if successful
					JSONObject resOb = new JSONObject(client.getResponse());
					return resOb.getInt("profile_id");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}

		
		@Override
		protected void onPostExecute(Integer result) {
			progressDialog.dismiss();
			if (result == null) {
				notifyConnectProblem();
			} else {
				// Set the online id to the current user
				currentUser.setOnlineId(result);
				app.saveCurrentUserStatistic();
				// Connect to the tcp server
				new CreateConnectThreadTask().execute();
			}
		}

	}
	
	/**
	 * Nested class to getting list of user from the server
	 * @author TheEmperor
	 *
	 */
	private class getListTask extends AsyncTask<String, Void, JSONArray> {

		@Override
		protected JSONArray doInBackground(String... params) {
			// Create the Rest Client
			RestClient client = new RestClient(params[0]);
			try {
				client.execute(RequestMethod.GET);
				if (client.getResponseCode() != 200) {
					notifyConnectProblem();
				} else {
					JSONObject resOb = new JSONObject(client.getResponse());
					JSONArray playersJSONArr = resOb.getJSONArray("players");
//					Log.d("JsonArray", profileJSONArr.toString());
					
					return playersJSONArr;
				}
			} catch (Exception e) {
				e.printStackTrace();
				notifyConnectProblem();
			}
			
			return null;
		}


		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
		
		
		@Override
		protected void onPostExecute(JSONArray result) {
			try {
				
				// Check changes in the player list in the server
				ArrayList<JSONObject> removedObjects = new ArrayList<JSONObject>();
				ArrayList<JSONObject> newObjects = new ArrayList<JSONObject>();
				
				// Check if some player is not in the list anymore
				for (JSONObject player : playersList) {
					boolean removed = true;
					for (int j = 0; j < result.length(); j++) {
						if (player.getInt("id") == result.getJSONObject(j).getInt("id")) {
							removed = false;
							break;
						}
					}
					
					if (removed) {
						removedObjects.add(player);
					}
				}
				
				// Remove the players in the local list
				playersList.removeAll(removedObjects);
				
				// Check if the list has some new player
				for (int i = 0; i < result.length(); i++) {
					boolean is_new = true;
					for (JSONObject player : playersList) {
						if (player.getInt("id") == result.getJSONObject(i).getInt("id")) {
							is_new = false;
							break;
						}
					}
					
					if (is_new) {
						newObjects.add(result.getJSONObject(i));
					}
				}
				
				// Add new players in the local list
				playersList.addAll(newObjects);
				// Update the note text
				noteText.setText("There is " + playersList.size() + " connected players");
				// Reload the list
				adapter.forceReload();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Static Handler object for updating the UI from the message 
	 * in the connection thread
	 */
	private Handler uiUpdater = new Handler() {
		public void handleMessage(Message msg) {
			String response = (String) msg.obj;
			
			// If there is some changes to the list of players, update the player list
			if (response.equals("new") || response.equals("connected") || response.endsWith("disconnected")) {
				String connectUrl = "http://203.162.16.44:9099/players";
				new getListTask().execute(connectUrl);
			}
			
			
			// If response message has more than one component
			if (response.contains(";")) {
				String[] resArr = response.split("\\;");
//				Log.d("response array", resArr.toString());
				
				// If there is a connect request from another player, display a dialog
				if (resArr[0].equals("connect")) {
					int senderId = Integer.parseInt(resArr[1]);
					notifyConnectDialog(getSenderName(senderId), senderId);
				}
				
				// If there is a reply from the connect request
				if (resArr[0].equals("reply")) {
					int senderId = Integer.parseInt(resArr[1]);
					String message = resArr[2];
					if (message.equals("ok")) {
						// When the player reply ok to connect, display the dialog
						notifyPlayReply(getSenderName(senderId), senderId, true);
					} else if (message.equals("decline")) {
						// When the player reply decline to connect, display the dialog
						notifyPlayReply(getSenderName(senderId), senderId, false);
					} else if (message.equals("cancel")) {
						// When the player reply cancel the gameplay, display the dialog
						notifyCancel(getSenderName(senderId));
					} else if (message.equals("busy")) {
						// When the player is in another channel and busy, display the dialog
						notifyBusy(getSenderName(senderId));
					} else if (message.equals("ready")) {
						// When everything is ready
						readyAndPlay();
					}
				} 
			}
		}
	};
	
	private String getSenderName(int senderId) {
		String senderName = "";
		
		for (JSONObject player : playersList) {
			try {
				if (player.getInt("id") == senderId) {
					senderName = player.getString("name");
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return senderName;
	}
	
	/**
	 * Nested class inherited from AsyncTask to create a connection to the TCP server
	 * @author TheEmperor
	 *
	 */
	private class CreateConnectThreadTask extends AsyncTask <Void, Integer, Void> {
		
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Connecting to the server");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				// Create a socket
				socket = new Socket(TCP_IP, Integer.parseInt(TCP_PORT));
				connectThread = new ConnectThread(socket);
				connectThread.start();
				
				// Send the username to server
				String idStr = Integer.toString(currentUser.getOnlineId());
				String request =  currentUser.getProfileName() + ";" + idStr;
				Log.d("request", request);
			    new WriteToServerTask().execute(request);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
		}
	}
	
	/**
	 * AsyncTask inherited class to sending message to server
	 * @author TheEmperor
	 *
	 */
	private class WriteToServerTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			connectThread.write(params[0]);
			return null;
		}
	}
	
	private class CloseSocketTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			connectThread.cancel();
			return null;
		}
		
	}
	
	
	/**
	 * Notify an alert dialog when there is a connection problem
	 */
	private void notifyConnectProblem() {
		problemTextDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.connection_problem)
		.setMessage(R.string.connection_problem_message)
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(MultiplayStartActivity.this, MainMenuActivity.class);
				startActivity(intent);
				MultiplayStartActivity.this.finish();
			}
		})
		.create();
		
		problemTextDialog.show();
	}
	
	/**
	 * Notify a dialog when a reply from the previous request come from the server
	 * @param senderName the name of the sender of the reply
	 * @param senderId	the sender id
	 * @param i the response is ok or not
	 */
	private void notifyPlayReply(final String senderName, final int senderId, boolean ok) {
		waitingDialog.dismiss();
		if (ok) {
			replyDialog = new AlertDialog.Builder(this)
			.setTitle(R.string.reply)
			.setMessage(senderName + " accepted your request")
			.setPositiveButton(R.string.play, new AlertDialog.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					String response = "reply;" + senderId + ";ready";
					new WriteToServerTask().execute(response);
					initializePlay();
				}
			})
			.setNegativeButton(R.string.cancel, new AlertDialog.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					String response = "reply;" + senderId + ";cancel";
					new WriteToServerTask().execute(response);
					replyDialog.cancel();
				}
			})
			.create();
		} else {
			replyDialog = new AlertDialog.Builder(this)
			.setTitle(R.string.reply)
			.setMessage(senderName + " decline your request")
			.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					replyDialog.cancel();
				}
			})
			.create();
		}
		
		replyDialog.show();
	}
	
	protected void initializePlay() {
		app.setSocket(socket);
		Intent intent = new Intent(MultiplayStartActivity.this, MultiplayMotorLiteActivity.class);
		startActivity(intent);
		connectThread.interrupt();
		this.finish();
	}

	private void notifyBusy(String senderName) {
		waitingDialog.dismiss();
		busyDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.decline)
		.setMessage(senderName + "is busy and cannot accept request")
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				busyDialog.cancel();
			}
		})
		.create();
		
		busyDialog.show();
	}
	
	private void notifyCancel(String senderName) {
		readyWaitingDialog.dismiss();
		cancelDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.decline)
		.setMessage(senderName + " cancelled the game")
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				cancelDialog.cancel();
			}
		})
		.create();
		
		cancelDialog.show();
	}
	
	protected void readyAndPlay() {
		readyWaitingDialog.dismiss();
		initializePlay();
	}

	
	/**
	 * Notify a dialog when a request to connect from another player come
	 */
	private void notifyConnectDialog(final String senderName, final int senderId) {
		connectTextDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.connect_request)
		.setMessage(senderName + " sent you a play request")
		.setPositiveButton(R.string.accept, new AlertDialog.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				readyWaitingDialog = ProgressDialog.show(MultiplayStartActivity.this, "Waiting to Play", "Waiting for " + senderName + " to be ready");
				String response = "reply;" + senderId + ";ok";
				new WriteToServerTask().execute(response);
				connectTextDialog.cancel();
			}
		})
		.setNegativeButton(R.string.decline, new AlertDialog.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				String response = "reply;" + senderId + ";decline";
				new WriteToServerTask().execute(response);
				connectTextDialog.cancel();
			}
		})
		.create();
		
		connectTextDialog.show();
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiplay_start);
		
		// Get the application instance
		app = (MotorLiteApplication)getApplication();
		// Get the currentUser profile
		currentUser = app.getCurrentUser();
		// Initialize the players array object
		playersList = new ArrayList<JSONObject>();
		
		// Set up view and callback
		setUpViews();
		// Create a handler to update list task
		
		// Set the list adapter to manage items in the list
		adapter = new OnlinePlayerAdapter(playersList, this);
		setListAdapter(adapter);
		
		// Create the progress dialog
		progressDialog = new ProgressDialog(this);
		
		// If the user doesn't have an online id, connect to http server and get the id
		// And use it to connect to the tcp server after that
		if (currentUser.getOnlineId() == 0) {
			Log.d("user online id", currentUser.getOnlineId()+"");
			String connectUrl = "http://dvelop.no.de/profiles";
			String token = "Basic YWRtaW46cGFzc3dvcmQ";
			
			// Create the progress Dialog
			
			// Start the async task to get the online id
			new InitialConnectTask().execute(connectUrl, token);
		} else {
			// Add the updateList task to the handler to update the list when the player list change
			new CreateConnectThreadTask().execute();
		}
		
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (currentUser.getOnlineId() != id) {
			new WriteToServerTask().execute("connect;" + id);
			waitingDialog = ProgressDialog.show(MultiplayStartActivity.this, "Waiting", "Waiting for response from the player");
		}
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		connectThread.cancel();
		this.finish();
	}


	private void setUpViews() {
		mainMenuButton = (Button)findViewById(R.id.main_menu_button);
		helloText = (TextView)findViewById(R.id.hello_text);
		noteText = (TextView)findViewById(R.id.note_text);
		
		// Set the click listener for buttons
		mainMenuButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent intent =  new Intent(MultiplayStartActivity.this, MainMenuActivity.class);
				startActivity(intent);
				MultiplayStartActivity.this.finish();
				new CloseSocketTask().execute();
				connectThread.interrupt();
			}
		});
		
		helloText.setText(helloText.getText().toString() + currentUser.getProfileName() + "!");
	}

}
