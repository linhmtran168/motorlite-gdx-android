package com.androidgame.motorlitegdx.mobile;

import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewProfileAcitivity extends Activity {

	private EditText profileText;
	private Button backButton;
	private Button confirmButton;
	protected boolean changesPending;
	private AlertDialog emptyTextDialog;
	private AlertDialog unsavedChangesDialog;
	private Dialog profileNameExistsDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_profile);
		setUpViews();
	}

	private void setUpViews() {
		profileText = (EditText)findViewById(R.id.profile_input);
		backButton = (Button)findViewById(R.id.back_button);
		confirmButton = (Button)findViewById(R.id.confirm_button);
		
		profileText.addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				changesPending = true;
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			public void afterTextChanged(Editable arg0) {
			}
		});
		
		backButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				cancel();
			}
		});
		
		confirmButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				String profileName = profileText.getText().toString().trim();
				if (!profileName.equals("")) {
					MotorLiteApplication app = (MotorLiteApplication)NewProfileAcitivity.this.getApplication();
					if (!app.doesProfileExist(profileName)) {
						confirm();
					} else {
						profileNameExists();
					}
				} else {
					emptyTextNotify();
				}
			}
		});
	}

	protected void profileNameExists() {
		profileNameExistsDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.profile_exists)
		.setMessage(R.string.profile_exists_message)
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				profileNameExistsDialog.cancel();
			}
		})
		.create();
		
		profileNameExistsDialog.show();
	}

	protected void emptyTextNotify() {
		emptyTextDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.empty_profile_name)
		.setMessage(R.string.empty_profile_name_message)
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				emptyTextDialog.cancel();
			}
		})
		.create();
		
		emptyTextDialog.show();
	}

	protected void confirm() {
		String profileName = profileText.getText().toString().trim();
		MotorLiteApplication app = (MotorLiteApplication)getApplication();
		app.setCurrentUser(profileName);
		Intent intent = new Intent(NewProfileAcitivity.this, MainMenuActivity.class);
		startActivity(intent);
		finish();
	}

	protected void cancel() {
		unsavedChangesDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.unsaved_changes)
		.setMessage(R.string.unsaved_changes_message)
		.setNeutralButton(R.string.discard, new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		})
		.create();
		
		unsavedChangesDialog.show();
	}

}
