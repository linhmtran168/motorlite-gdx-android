package com.androidgame.motorlitegdx.mobile;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.adapter.OnlineTopListAdapter;
import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;
import com.androidgame.motorlitegdx.mobile.helpers.RequestMethod;
import com.androidgame.motorlitegdx.mobile.helpers.RestClient;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class OnlineStatisticActivity extends ListActivity {

	public static final String SERVICE_URI = "http://dvelop.no.de/profiles";
	public static final String TOKEN = "Basic YWRtaW46cGFzc3dvcmQ";
	
	private MotorLiteApplication app;
	private Button mainMenuButton;
	private TextView bestTimeText;
	private TextView loseText;
	private TextView winText;
	private TextView profileName;
	private TextView rankText;
	private UserProfile currentUser;
	private ProgressDialog progressDialog;
	private int userRank;
	private ArrayList<JSONObject> profileJSONlist;
	private OnlineTopListAdapter adapter;
	protected AlertDialog problemTextDialog;

	
	private class SetProfileTask extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Update your statistic");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (currentUser.getOnlineId() == 0) {
				RestClient client = new RestClient(SERVICE_URI);
				client.addHeader("Authorization", TOKEN);
				client.addParam("name", currentUser.getProfileName());
				client.addParam("win", currentUser.getWin().toString());
				client.addParam("lose", currentUser.getLose().toString());
				client.addParam("best_time", currentUser.getBestTime().toString());
				try {
					client.execute(RequestMethod.POST);
					if (client.getResponseCode() != 200) {
						notifyConnectProblem();
					} else {
						JSONObject resOb = new JSONObject(client.getResponse());
						currentUser.setOnlineId(resOb.getInt("profile_id"));
						app.saveCurrentUserStatistic();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				RestClient client = new RestClient(SERVICE_URI + "/" + currentUser.getOnlineId());
				client.addHeader("Authorization", TOKEN);
				client.addParam("name", currentUser.getProfileName());
				client.addParam("win", currentUser.getWin().toString());
				client.addParam("lose", currentUser.getLose().toString());
				client.addParam("best_time", currentUser.getBestTime().toString());
				try {
					client.execute(RequestMethod.PUT);
					if (client.getResponseCode() != 200) {
						notifyConnectProblem();
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result); 
			progressDialog.dismiss();
			new GetTopProfilesTask().execute();
		}
	}
	
	private class GetTopProfilesTask extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Getting list of top online profiles");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			RestClient client = new RestClient(SERVICE_URI + "/top" + "/10");
			client.addHeader("Authorization", TOKEN);
			try {
				client.execute(RequestMethod.GET);
				if (client.getResponseCode() != 200) {
					notifyConnectProblem();
				} else {
					JSONObject resOb = new JSONObject(client.getResponse());
					JSONArray profileJSONArr = resOb.getJSONArray("profiles");
	//				Log.d("JsonArray", profileJSONArr.toString());
					for (int i = 0; i < profileJSONArr.length(); i++) {
						profileJSONlist.add(profileJSONArr.getJSONObject(i));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		protected void onPostExecute(Void result) {
			adapter = new OnlineTopListAdapter(profileJSONlist, OnlineStatisticActivity.this);
			setListAdapter(adapter);
			progressDialog.dismiss();
		}
		
	}
	
	private class GetUserProfileTask extends AsyncTask<Void, Void, Void> {
		
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog.setTitle("Loading");
			progressDialog.setMessage("Getting your statistic from the server");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			RestClient client = new RestClient(SERVICE_URI + "/" + currentUser.getOnlineId());
			client.addHeader("Authorization", TOKEN);
			try {
				client.execute(RequestMethod.GET);
				if (client.getResponseCode() != 200) {
					notifyConnectProblem();
				} else {
					JSONObject resOb = new JSONObject(client.getResponse());
					JSONObject userJSONob = resOb.getJSONArray("profile").getJSONObject(0);
	//				Log.d("JSON response", userJSONob.toString());
					userRank = userJSONob.getInt("rank");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			profileName.setText(profileName.getText().toString()+currentUser.getProfileName());
			winText.setText(winText.getText().toString()+currentUser.getWin());
			loseText.setText(loseText.getText().toString()+currentUser.getLose());
			bestTimeText.setText(bestTimeText.getText().toString()+currentUser.getBestTime());
			if (userRank != 0) {
				rankText.setText(rankText.getText().toString() + userRank);
			} else {
				rankText.setText(rankText.getText().toString() + "Couldn't get rank");
			}
		}
	}
	
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.online_statistic);
		setUpViews();
		
		app = (MotorLiteApplication)getApplication();
		currentUser = app.getCurrentUser();
		profileJSONlist = new ArrayList<JSONObject>();
		
		progressDialog = new ProgressDialog(this);
		
		new SetProfileTask().execute();
		
//		Log.d("profileLIST", profileJSONlist.toString());
	}


	@Override
	protected void onResume() {
		super.onResume();
		
		new GetUserProfileTask().execute();
		
	}
	
	private void notifyConnectProblem() {
		problemTextDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.connection_problem)
		.setMessage(R.string.connection_problem_message)
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(OnlineStatisticActivity.this, MainMenuActivity.class);
				startActivity(intent);
				OnlineStatisticActivity.this.finish();
			}
		})
		.create();
		
		problemTextDialog.show();
	}

	private void setUpViews() {
		mainMenuButton = (Button)findViewById(R.id.main_menu_button);
		profileName = (TextView)findViewById(R.id.profile_name);
		winText = (TextView)findViewById(R.id.win);
		loseText = (TextView)findViewById(R.id.lose);
		bestTimeText = (TextView)findViewById(R.id.best_time);
		rankText = (TextView)findViewById(R.id.rank);
		
		mainMenuButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(OnlineStatisticActivity.this, MainMenuActivity.class);
				startActivity(intent);
				OnlineStatisticActivity.this.finish();
			}
		});
	}

}
