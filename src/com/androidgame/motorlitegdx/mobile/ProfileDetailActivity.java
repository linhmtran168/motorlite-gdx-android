package com.androidgame.motorlitegdx.mobile;

import static com.androidgame.motorlitegdx.mobile.OnlineStatisticActivity.SERVICE_URI;
import static com.androidgame.motorlitegdx.mobile.OnlineStatisticActivity.TOKEN;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;
import com.androidgame.motorlitegdx.mobile.helpers.RequestMethod;
import com.androidgame.motorlitegdx.mobile.helpers.RestClient;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class ProfileDetailActivity extends Activity {

	private Button setActiveButton;
	private UserProfile selectedUser;
	private TextView bestTimeText;
	private TextView loseText;
	private TextView winText;
	private TextView profileNameText;
	private Button deleteButton;
	private MotorLiteApplication app;
	private TextView notifyText;
	private Button backButton;
	private AlertDialog problemTextDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_detail);
		int userPosition = getIntent().getIntExtra("position", 0);
		app = (MotorLiteApplication)getApplication();
		selectedUser = app.getProfileList().get(userPosition);
		setUpViews();
	}


	private void setUpViews() {
		setActiveButton = (Button)findViewById(R.id.set_active_button);
		deleteButton = (Button)findViewById(R.id.delete_button);
		backButton = (Button)findViewById(R.id.back_button);
		profileNameText = (TextView)findViewById(R.id.profile_name);
		winText = (TextView)findViewById(R.id.win);
		loseText = (TextView)findViewById(R.id.lose);
		bestTimeText = (TextView)findViewById(R.id.best_time);
		notifyText = (TextView)findViewById(R.id.notify_text);
		
		if (selectedUser.isActive()) {
			setActiveButton.setVisibility(View.GONE);
			deleteButton.setVisibility(View.GONE);
			notifyText.setVisibility(View.VISIBLE);
		}
		setActiveButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				ProfileDetailActivity.this.app.setCurrentUser(ProfileDetailActivity.this.selectedUser);
				ProfileDetailActivity.this.finish();
			}
		});
		
		deleteButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				deleteUserProfile();
			}
		});
		
		backButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				ProfileDetailActivity.this.finish();
			}
		});
	}


	protected void deleteUserProfile() {
		// TODO Auto-generated method stub
		if (selectedUser.getOnlineId() == 0)  {
			app.deleteUserProfile(selectedUser);
			this.finish();
		} else {
			RestClient client = new RestClient(SERVICE_URI + "/" + selectedUser.getOnlineId());
			client.addHeader("Authorization", TOKEN);
			try {
				client.execute(RequestMethod.DELETE);
				if (client.getResponseCode() != 200) {
					notifyConnectProblem();
				} else {
					app.deleteUserProfile(selectedUser);
					this.finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	private void notifyConnectProblem() {
		problemTextDialog = new AlertDialog.Builder(this)
		.setTitle(R.string.delete_problem)
		.setMessage(R.string.delete_problem_message)
		.setNeutralButton(android.R.string.ok, new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				problemTextDialog.cancel();
			}
		})
		.create();
		
		problemTextDialog.show();
		
	}


	@Override
	protected void onResume() {
		super.onResume();
		profileNameText.setText(profileNameText.getText().toString() + selectedUser.getProfileName());
		winText.setText(winText.getText().toString() + selectedUser.getWin());
		loseText.setText(loseText.getText().toString() + selectedUser.getLose());
		bestTimeText.setText(bestTimeText.getText().toString() + selectedUser.getBestTime());
	}
	
}
