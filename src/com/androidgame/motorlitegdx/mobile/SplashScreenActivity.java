package com.androidgame.motorlitegdx.mobile;

import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		setUpNextActivity();
	}

	private void setUpNextActivity() {
		MotorLiteApplication app = (MotorLiteApplication)getApplication();
		if (app.getCurrentUser() != null) {
			new Handler().postDelayed(new Thread() {
				public void run() {
					Intent intent = new Intent(SplashScreenActivity.this, MainMenuActivity.class);
					SplashScreenActivity.this.startActivity(intent);
					SplashScreenActivity.this.finish();
					overridePendingTransition(R.layout.fade_in, R.layout.fade_out);
				}
			}, 4000);
		} else {
			new Handler().postDelayed(new Thread() {
				public void run() {
					Intent intent = new Intent(SplashScreenActivity.this, NewProfileAcitivity.class);
					SplashScreenActivity.this.startActivity(intent);
					SplashScreenActivity.this.finish();
					overridePendingTransition(R.layout.fade_in, R.layout.fade_out);
				}
			}, 4000);
		}
	}

}
