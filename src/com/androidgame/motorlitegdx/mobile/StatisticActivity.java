package com.androidgame.motorlitegdx.mobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class StatisticActivity extends Activity {

	private Button mainMenuButton;
	private UserProfile currentUser;
	private TextView bestTimeText;
	private TextView loseText;
	private TextView winText;
	private TextView profileNameText;
	private Button onlineStatButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.statistic);
		MotorLiteApplication app = ((MotorLiteApplication)getApplication());
		app.saveCurrentUserStatistic();
		currentUser = app.getCurrentUser();
		setUpViews();
	}


	private void setUpViews() {
		mainMenuButton = (Button)findViewById(R.id.main_menu_button);
		onlineStatButton = (Button)findViewById(R.id.online_stat_button);
		profileNameText = (TextView)findViewById(R.id.profile_name);
		winText = (TextView)findViewById(R.id.win);
		loseText = (TextView)findViewById(R.id.lose);
		bestTimeText = (TextView)findViewById(R.id.best_time);
		
		mainMenuButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(StatisticActivity.this, MainMenuActivity.class);
				startActivity(intent);
				StatisticActivity.this.finish();
			}
		});
		
		onlineStatButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(StatisticActivity.this, OnlineStatisticActivity.class);
				startActivity(intent);
				StatisticActivity.this.finish();
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
		profileNameText.setText(profileNameText.getText().toString() + currentUser.getProfileName());
		winText.setText(winText.getText().toString() + currentUser.getWin());
		loseText.setText(loseText.getText().toString() + currentUser.getLose());
		bestTimeText.setText(bestTimeText.getText().toString() + currentUser.getBestTime());
	}
	
}
