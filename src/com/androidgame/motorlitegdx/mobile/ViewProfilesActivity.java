package com.androidgame.motorlitegdx.mobile;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.androidgame.motorlitegdx.mobile.adapter.ProfileListAdapter;
import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;

public class ViewProfilesActivity extends ListActivity {

	private MotorLiteApplication app;
	private ProfileListAdapter adapter;
	private Button newProfileButton;
	private Button mainMenuButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_list);
		setUpViews();
		
		app = (MotorLiteApplication)getApplication();
		adapter = new ProfileListAdapter(app.getProfileList(), this);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(this, ProfileDetailActivity.class);
		intent.putExtra("position", position);
		startActivity(intent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		adapter.reload();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		adapter.reload();
	}

	private void setUpViews() {
		newProfileButton = (Button)findViewById(R.id.create_profile_button);
		mainMenuButton = (Button)findViewById(R.id.main_menu_button);
		
		newProfileButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(ViewProfilesActivity.this, NewProfileAcitivity.class);
				startActivity(intent);
			}
		});
		
		mainMenuButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				finish();
			}
		});
	}

}
