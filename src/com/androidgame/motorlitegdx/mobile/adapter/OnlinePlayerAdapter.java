package com.androidgame.motorlitegdx.mobile.adapter;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.R;

public class OnlinePlayerAdapter extends BaseAdapter implements ListAdapter {

	private Activity context;
	private ArrayList<JSONObject> players;

	public OnlinePlayerAdapter(ArrayList<JSONObject> players, Activity context) { // TODO Auto-generated constructor stub
		this.players = players;
		this.context = context;
	}

	public int getCount() {
		return players.size();
	}

	public Object getItem(int position) {
		return (null == players) ? null : players.get(position);
	}

	public long getItemId(int position) {
		try {
			return players.get(position).getLong("id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_row_3, null, true);
		TextView nameTextView = (TextView) rowView.findViewById(R.id.player_name);
		TextView idTextView = (TextView) rowView.findViewById(R.id.online_id);
		
		try {
			nameTextView.setText(players.get(position).getString("name"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			idTextView.setText(players.get(position).getString("id"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rowView;
	}
	
	public void forceReload() {
		notifyDataSetChanged();
	}

}
