package com.androidgame.motorlitegdx.mobile.adapter;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.R;

public class OnlineTopListAdapter extends BaseAdapter {

	private ArrayList<JSONObject> profiles;
	private Activity context;

	public OnlineTopListAdapter(ArrayList<JSONObject> profileJSONlist, Activity context) {
		super();
		this.profiles = profileJSONlist;
		this.context = context;
	}

	public int getCount() {
		return profiles.size();
	}

	public Object getItem(int position) {
		return (null == profiles) ? null : profiles.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_row_2, null, true);
		TextView rankTextView = (TextView) rowView.findViewById(R.id.rank);
		TextView nameTextView = (TextView) rowView.findViewById(R.id.profile_name);
		TextView winTextView = (TextView) rowView.findViewById(R.id.win_num);
		TextView loseTextView = (TextView) rowView.findViewById(R.id.lose_num);
		
		rankTextView.setText((position+1)+".");
		try {
			nameTextView.setText(profiles.get(position).getString("name"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			winTextView.setText("Win: " + profiles.get(position).getString("win"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			loseTextView.setText("Lose: " + profiles.get(position).getString("lose"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rowView;
	}

}
