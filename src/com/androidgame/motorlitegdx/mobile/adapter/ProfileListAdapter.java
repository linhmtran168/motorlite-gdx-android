package com.androidgame.motorlitegdx.mobile.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidgame.motorlitegdx.mobile.R;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class ProfileListAdapter extends BaseAdapter {

	private ArrayList<UserProfile> profiles;
	private Activity context;

	public ProfileListAdapter(ArrayList<UserProfile> profiles, Activity context) {
		super();
		this.profiles = profiles;
		this.context = context;
	}

	public int getCount() {
		return profiles.size();
	}

	public Object getItem(int position) {
		return (null == profiles) ? null : profiles.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_row, null, true);
		TextView textView = (TextView) rowView.findViewById(R.id.profile_label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.active_image);
		textView.setText(profiles.get(position).getProfileName());
		if (profiles.get(position).isActive()) {
			imageView.setVisibility(View.VISIBLE);
		}
		
		return rowView;
	}

	public void reload() {
		notifyDataSetChanged();
	}
}
