package com.androidgame.motorlitegdx.mobile.controller;

import java.net.Socket;
import java.util.ArrayList;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class MotorLiteApplication extends Application {

	private SQLiteDatabase db;
	private ProfileController profileController;
	private UserProfile currentUser;
	private ArrayList<UserProfile> profileList;
	private Socket socket;
	@Override
	public void onCreate() {
		super.onCreate();
		StatSQLiteHelper sqliteHelper = new StatSQLiteHelper(this);
		db = sqliteHelper.getWritableDatabase();
		profileController = new ProfileController(db);
		setProfileList(profileController.loadProfiles());
		if (profileList != null) {
			setActiveUser();
		}
	}

	private void setActiveUser() {
		for (UserProfile profile : profileList) {
			if (profile.isActive()) {
				currentUser = profile;
			}
		}
	}

	/**
	 * @return the currentUser
	 */
	public UserProfile getCurrentUser() {
		return currentUser;
	}

	/**
	 * @param currentUser the currentUser to set
	 */
	public void setCurrentUser(UserProfile user) {
		if (currentUser != null) {
			deactiveCurrentUser();
		}
		user.setActive(true);
		profileController.saveUser(user);
		this.currentUser = user;
	}
	
	public void setCurrentUser(String profileName) {
		if (currentUser != null) {
			deactiveCurrentUser();
		}
		
		// Create the new user profile
		currentUser = new UserProfile(0, 0, profileName, 0, 0, 0, true);
		currentUser.setId(profileController.addUserProfile(currentUser));
		profileList.add(currentUser);
	}

	private void deactiveCurrentUser() {
		currentUser.setActive(false);
		profileController.saveUser(currentUser);
	}
	
	public void saveCurrentUserStatistic() {
		profileController.saveUser(currentUser);
	}
	
	public boolean doesProfileExist(String profileName) {
		return profileController.doesProfileExist(profileName);
	}
	
	
	public UserProfile getUserProfile(int id) {
		return profileController.getProfile(id);
	}
	
	public void deleteUserProfile(UserProfile profile) {
		profileList.remove(profile);
		profileController.deleteProfile(profile.getId());
	}

	/**
	 * @return the profileList
	 */
	public ArrayList<UserProfile> getProfileList() {
		return profileList;
	}

	/**
	 * @param profileList the profileList to set
	 */
	public void setProfileList(ArrayList<UserProfile> profileList) {
		this.profileList = profileList;
	}

	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * @param socket the socket to set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
}
