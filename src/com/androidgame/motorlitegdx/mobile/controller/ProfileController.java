package com.androidgame.motorlitegdx.mobile.controller;

import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.LAST_ACTIVE;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.ONLINE_ID;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_BEST;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_ID;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_LOSE;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_PROFILE;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_TABLE;
import static com.androidgame.motorlitegdx.mobile.helpers.StatSQLiteHelper.USER_WIN;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class ProfileController {

	private SQLiteDatabase db;

	public ProfileController(SQLiteDatabase db) {
		this.db = db; 
	}
	
	public UserProfile getLastActiveProfile() {
		UserProfile userProfile;
		Cursor cursor = db.query(
				USER_TABLE,
				new String[] {USER_ID, USER_PROFILE, USER_WIN, USER_LOSE, USER_BEST, LAST_ACTIVE, ONLINE_ID},
				LAST_ACTIVE +" = ?",
				new String[] {"true"},
				null,
				null,
				null
				);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			int id = cursor.getInt(0);
			String profileName = cursor.getString(1);
			int win = cursor.getInt(2);
			int lose = cursor.getInt(3);
			float bestTime = cursor.getFloat(4);
			boolean isActive = Boolean.parseBoolean(cursor.getString(5));
			int onlineId = cursor.getInt(6);
			userProfile = new UserProfile(id, onlineId, profileName, win, lose, bestTime, isActive);
			cursor.close();
			return userProfile;
		}
				
		cursor.close();
		return null;
	}
	
	public UserProfile getProfile(int profileId) {
		UserProfile userProfile;
		Cursor cursor = db.query(
				USER_TABLE,
				new String[] {USER_ID, USER_PROFILE, USER_WIN, USER_LOSE, USER_BEST, LAST_ACTIVE, ONLINE_ID},
				USER_ID +" = ?",
				new String[] {profileId+""},
				null,
				null,
				null
				);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			int id = cursor.getInt(0);
			String profileName = cursor.getString(1);
			int win = cursor.getInt(2);
			int lose = cursor.getInt(3);
			float bestTime = cursor.getFloat(4);
			boolean isActive = Boolean.parseBoolean(cursor.getString(5));
			int onlineId = cursor.getInt(6);
			userProfile = new UserProfile(id, onlineId, profileName, win, lose, bestTime, isActive);
			cursor.close();
			return userProfile;
		}
				
		cursor.close();
		return null;
	}

	public boolean doesProfileExist(String profileName) {
		Cursor cursor = db.query(
				USER_TABLE,
				new String[] {USER_ID},
				USER_PROFILE + " = ?",
				new String[] {profileName},
				null,
				null,
				null
				);
		
		if (cursor.getCount() > 0) {
			return true;
		}
		
		return false;
	}

	public int addUserProfile(UserProfile currentUser) {
		assert(null != currentUser);
		
		ContentValues values = new ContentValues();
		values.put(USER_PROFILE, currentUser.getProfileName());
		values.put(USER_WIN, currentUser.getWin());
		values.put(USER_LOSE, currentUser.getLose());
		values.put(USER_BEST, currentUser.getBestTime());
		values.put(LAST_ACTIVE, Boolean.toString(currentUser.isActive()));
		
		return (int)db.insert(USER_TABLE, null, values);
	}

	public void saveUser(UserProfile currentUser) {
		assert(null != currentUser);
		
		ContentValues values = new ContentValues();
		values.put(USER_PROFILE, currentUser.getProfileName());
		values.put(USER_WIN, currentUser.getWin());
		values.put(USER_LOSE, currentUser.getLose());
		values.put(USER_BEST, currentUser.getBestTime());
		values.put(LAST_ACTIVE, Boolean.toString(currentUser.isActive()));
		values.put(ONLINE_ID, currentUser.getOnlineId());
		
		long id = currentUser.getId();
		String where = String.format("%s = ?", USER_ID);
		
		db.update(USER_TABLE, values, where, new String[] {id+""});
	}

	public ArrayList<UserProfile> loadProfiles() {
		ArrayList<UserProfile> profileLists = new ArrayList<UserProfile>();
		UserProfile userProfile;
		
		Cursor cursor = db.query(
				USER_TABLE,
				new String[] {USER_ID, USER_PROFILE, USER_WIN, USER_LOSE, USER_BEST, LAST_ACTIVE, ONLINE_ID},
				null,
				null,
				null,
				null,
				String.format("%s,%s", USER_PROFILE,LAST_ACTIVE)
				);
		
		cursor.moveToFirst();

		if (!cursor.isAfterLast()) {
			do {
				int id = cursor.getInt(0);
				String profileName = cursor.getString(1);
				int win = cursor.getInt(2);
				int lose = cursor.getInt(3);
				float bestTime = cursor.getFloat(4);
				boolean isActive = Boolean.parseBoolean(cursor.getString(5));
				int onlineId = cursor.getInt(6); 
				userProfile = new UserProfile(id, onlineId, profileName, win, lose, bestTime, isActive);
				profileLists.add(userProfile);
			} while (cursor.moveToNext());
		}
				
		cursor.close();
		return profileLists;
	}

	public void deleteProfile(int id) {
		String where = String.format("%s = ?", USER_ID);
		db.delete(USER_TABLE, where, new String[] {""+id});
	}
}