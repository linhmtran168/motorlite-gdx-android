package com.androidgame.motorlitegdx.mobile.helpers;

import java.net.Socket;

import android.app.Activity;
import android.content.Intent;

import com.androidgame.motorlitegdx.mobile.MainMenuActivity;
import com.androidgame.motorlitegdx.mobile.StatisticActivity;
import com.androidgame.motorlitegdx.mobile.controller.MotorLiteApplication;
import com.androidgame.motorlitegdx.mobile.model.UserProfile;

public class ActionResolverAndroid {
	Activity appActivity;
	
	public ActionResolverAndroid(Activity appActivity) {
		this.appActivity = appActivity;
	}
	
	public void quitGDXGame(boolean isFinished) {
		// Quite the Game Thread to return to the UI Thread
		if (isFinished) {
			appActivity.startActivity(new Intent(this.appActivity, StatisticActivity.class));
			this.appActivity.finish();
		} else {
			appActivity.startActivity(new Intent(this.appActivity, MainMenuActivity.class));
			this.appActivity.finish();
		}
	}

	public void saveUserScore(boolean isWinner, float time) {
		UserProfile currentUser = ((MotorLiteApplication)appActivity.getApplication()).getCurrentUser();
		if (isWinner) {
			currentUser.setWin(currentUser.getWin()+1);
		} else {
			currentUser.setLose(currentUser.getLose()+1);
		}
		
		if (currentUser.getBestTime() > 0) {
			if (time < currentUser.getBestTime()) {
				currentUser.setBestTime(time);
			}
		} else {
			currentUser.setBestTime(time);
		}
	}
	
	public Socket getAndroidSocket() {
		return ((MotorLiteApplication)appActivity.getApplication()).getSocket();
	}
}
