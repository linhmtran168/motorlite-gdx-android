package com.androidgame.motorlitegdx.mobile.helpers;

public enum RequestMethod {
	GET,
	POST,
	PUT,
	DELETE
}
