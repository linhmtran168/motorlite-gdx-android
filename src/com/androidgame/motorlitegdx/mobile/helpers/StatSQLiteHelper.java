package com.androidgame.motorlitegdx.mobile.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class StatSQLiteHelper extends SQLiteOpenHelper {
	
	public static final String DB_NAME = "user_db.sqlite";
	public static final int VERSION = 1;
	public static final String USER_TABLE = "user_stat";
	public static final String USER_ID = "id";
	public static final String USER_PROFILE = "profile";
	public static final String USER_WIN = "win";
	public static final String USER_LOSE = "lose";
	public static final String USER_BEST = "best_time";
	public static final String LAST_ACTIVE = "last_active";
	public static final String ONLINE_ID = "online_id";
	
	public StatSQLiteHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(
				"create table " + USER_TABLE + " (" +
				USER_ID + " integer primary key autoincrement not null," +
				USER_PROFILE + " text not null," +
				USER_WIN + " integer," +
				USER_LOSE + " integer," +
				USER_BEST + " real," +
				LAST_ACTIVE + " text," +
				ONLINE_ID + " integer" +
				");"
				);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	}

}
