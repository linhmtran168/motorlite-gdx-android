package com.androidgame.motorlitegdx.mobile.helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class TCPConnectThread extends Thread {
	
	private final Socket socket;
	private final BufferedReader reader;
	private final BufferedWriter writer;
	private String message;
	
	public TCPConnectThread(Socket sock) {
		socket = sock;
		BufferedReader tmpReader = null;
		BufferedWriter tmpWriter = null;
		message = "";
		
		try {
			tmpReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			tmpWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		reader = tmpReader;
		writer = tmpWriter;
	}
	
	/**
	 * Keep listening to the InputStream to receive message
	 * until an exception occurs
	 */
	public void run() {
		String line = "";
		
		while (!Thread.currentThread().isInterrupted()) {
			try {
				// Read line from the input stream
				line = reader.readLine();
				// Update the message field 
				message = line;
				System.out.println("response: " +  message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public synchronized String getMessage() {
		return message;
	}
	
	public void write(String line) {
		try {
			// Write line to the TCP server
			writer.write(line+"\n", 0, line.length() + 1);
			writer.flush();
			System.out.println("write line: " + line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void cancel() {
		try {
			socket.close();
			Thread.currentThread().interrupt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
