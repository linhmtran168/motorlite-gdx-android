package com.androidgame.motorlitegdx.mobile.model;

public class UserProfile {
	
	private boolean isActive;
	private float bestTime;
	private int lose;
	private int win;
	private String profileName;
	private int id;
	private int onlineId;

	public UserProfile(int id, int onlineId, String profileName, int win,
			int lose, float bestTime, boolean isActive) {
		this.setOnlineId(onlineId);
		this.setId(id);
		this.setProfileName(profileName);
		this.setWin(win);
		this.setLose(lose);
		this.setBestTime(bestTime);
		this.setActive(isActive);
	}

	public UserProfile() {
		
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * @return the win
	 */
	public Integer getWin() {
		return win;
	}

	/**
	 * @param win the win to set
	 */
	public void setWin(Integer win) {
		this.win = win;
	}

	/**
	 * @return the lose
	 */
	public Integer getLose() {
		return lose;
	}

	/**
	 * @param lose the lose to set
	 */
	public void setLose(Integer lose) {
		this.lose = lose;
	}

	/**
	 * @return the bestTime
	 */
	public Float getBestTime() {
		return bestTime;
	}

	/**
	 * @param bestTime the bestTime to set
	 */
	public void setBestTime(Float bestTime) {
		this.bestTime = bestTime;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the onlineId
	 */
	public int getOnlineId() {
		return onlineId;
	}

	/**
	 * @param onlineId the onlineId to set
	 */
	public void setOnlineId(int onlineId) {
		this.onlineId = onlineId;
	}
}
